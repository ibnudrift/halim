-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 22, 2020 at 10:30 PM
-- Server version: 10.3.24-MariaDB-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `richmore_halim`
--

-- --------------------------------------------------------

--
-- Table structure for table `gal_gallery`
--

CREATE TABLE `gal_gallery` (
  `id` int(11) NOT NULL,
  `topik_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `image2` varchar(225) DEFAULT NULL,
  `active` int(11) NOT NULL,
  `date_input` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `insert_by` varchar(255) NOT NULL,
  `last_update_by` varchar(255) NOT NULL,
  `writer` varchar(200) NOT NULL,
  `city` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gal_gallery`
--

INSERT INTO `gal_gallery` (`id`, `topik_id`, `image`, `image2`, `active`, `date_input`, `date_update`, `insert_by`, `last_update_by`, `writer`, `city`) VALUES
(1, 1, 'f3d3e-cover-dining-kitchen-nippo.jpg', 'f3d3e-dining-kitchen-nippo-003.jpg', 1, '2019-12-25 21:30:34', '2019-12-27 10:39:26', 'info@markdesign.net', 'info@markdesign.net', '', ''),
(2, 1, '458b7-cover-bedroom-livingroom-nippo.jpg', '00f13-Layer-2.jpg', 1, '2019-12-26 10:34:24', '2020-02-10 10:11:31', 'info@markdesign.net', 'info@markdesign.net', '', ''),
(3, 1, '2d13a-cover-nippo-doors.jpg', '97aeb-thumb_c9018-layer-3_adaptiveResize_403_465.jpg', 1, '2019-12-26 10:39:21', '2020-02-10 10:10:45', 'info@markdesign.net', 'info@markdesign.net', '', ''),
(4, 1, 'db7ba-cover-nippo-office-photography.jpg', 'db7ba-office-photography-001.jpg', 1, '2019-12-26 10:40:08', '2020-02-10 10:11:12', 'info@markdesign.net', 'info@markdesign.net', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `gal_gallery_description`
--

CREATE TABLE `gal_gallery_description` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `sub_title` text NOT NULL,
  `content` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gal_gallery_description`
--

INSERT INTO `gal_gallery_description` (`id`, `gallery_id`, `language_id`, `title`, `sub_title`, `content`) VALUES
(7, 1, 2, 'DINING & KITCHEN', '<p>Nippo Home Furnishing Gallery in Surabaya presents the high quality luxury Dining & Kitchen furniture for your special home. You will expect only superb quality on every detail of our furnitures. Furnitures build by Nippo Techs are a piece of investment that combines both modern machine technology and high craftsmanship quality. We pride ourself as the best because we believe our customers deserve only the best.</p>', '<p>Come to Nippo Gallery to shop your luxurious Dining & Kitchen furniture in Surabaya. We are the leading home furnishing gallery in Surabaya providing bespoke furniture made by Nippo Techs factory and also provide collections of world’s best designer brand such as Giorgetti, Zeyko, etc.</p>'),
(15, 2, 2, 'BEDROOM & LIVING ROOM', '<p>Nippo Home Furnishing Gallery in Surabaya presents the high quality luxury Bedroom & Livingroom furniture for your special home. You will expect only superb quality on every detail of our bedframes, bed side table, wardrobe cabinetries and a comprehensive leather upholstery - sofa from our Italian curated collection. Bedroom & Livingroom Furnitures build by Nippo Techs are a piece of investment that combines both modern machine technology and high craftsmanship quality. We pride ourself as the best because we believe our customers deserve only the best.</p>', '<p>Come to Nippo Gallery to shop your luxurious Dining & Kitchen furniture in Surabaya. We are the leading home furnishing gallery in Surabaya providing bespoke furniture made by Nippo Techs factory and also provide collections of world’s best designer brand such as Giorgetti, Zeyko, etc.</p>'),
(13, 3, 2, 'DOORS', '<p>Nippo Home Furnishing Gallery in Surabaya presents the high detailed and sophisticated technology of luxury quality door making. We have worked with International and reputable hotels, apartments and luxury homes to provide thousands of systematics and custom doors. You will expect only superb quality on every detail of our doors. Doors build by Nippo Techs are a piece of investment that combines both modern machine technology and high craftsmanship quality. We pride ourself as the best because we believe our customers deserve only the best.\r\n</p>', '<p>Come to Nippo Gallery to shop your luxurious Dining & Kitchen furniture in Surabaya. We are the leading home furnishing gallery in Surabaya providing bespoke furniture made by Nippo Techs factory and also provide collections of world’s best designer brand such as Giorgetti, Zeyko, etc.</p>'),
(14, 4, 2, 'OFFICE', '<p>Nippo Home Furnishing Gallery in Surabaya presents the high quality luxury Modern & Contemporary Office furniture for you. You will expect only superb quality on every detail of our office furnitures. Office furnitures build by Nippo Techs are a piece of investment that combines both modern machine technology and high craftsmanship quality. We pride ourself as the best because we believe our customers deserve only the best.</p>', '<p>Come to Nippo Gallery to shop your luxurious Dining & Kitchen furniture in Surabaya. We are the leading home furnishing gallery in Surabaya providing bespoke furniture made by Nippo Techs factory and also provide collections of world’s best designer brand such as Giorgetti, Zeyko, etc.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `gal_gallery_image`
--

CREATE TABLE `gal_gallery_image` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `image` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gal_gallery_image`
--

INSERT INTO `gal_gallery_image` (`id`, `gallery_id`, `image`) VALUES
(24, 1, '2e8da-dining-kitchen-nippo-002.jpg'),
(23, 1, 'f3d3e-dining-kitchen-nippo-001.jpg'),
(72, 2, '458b7-bedroom-livingroom-nippo-005.jpg'),
(65, 3, '07b68-nippo-doors-008.jpg'),
(64, 3, '07b68-nippo-doors-007.jpg'),
(67, 4, 'f1f1e-nippo-office-photography-003.jpg'),
(63, 3, '07b68-nippo-doors-006.jpg'),
(62, 3, '07b68-nippo-doors-005.jpg'),
(61, 3, '07b68-nippo-doors-004.jpg'),
(60, 3, '07b68-nippo-doors-003.jpg'),
(59, 3, '07b68-nippo-doors-002.jpg'),
(58, 3, '07b68-nippo-doors-001.jpg'),
(71, 2, '458b7-bedroom-livingroom-nippo-004.jpg'),
(70, 2, '458b7-bedroom-livingroom-nippo-003.jpg'),
(69, 2, '458b7-bedroom-livingroom-nippo-002.jpg'),
(68, 2, '458b7-bedroom-livingroom-nippo-001.jpg'),
(25, 1, '2e8da-dining-kitchen-nippo-004.jpg'),
(26, 1, '2e8da-dining-kitchen-nippo-005.jpg'),
(66, 4, 'db7ba-nippo-office-photography-002.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `code` varchar(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `status` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `name`, `code`, `sort`, `status`) VALUES
(2, 'English', 'en', 1, '1'),
(3, 'Indonesia', 'id', 2, '0');

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `activity` varchar(100) NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log`
--

INSERT INTO `log` (`id`, `username`, `activity`, `time`) VALUES
(1, 'info@markdesign.net', 'Login: info@markdesign.net', '2020-06-16 12:10:26'),
(2, 'info@markdesign.net', 'Slide Controller Create 1', '2020-06-16 12:17:22');

-- --------------------------------------------------------

--
-- Table structure for table `me_member`
--

CREATE TABLE `me_member` (
  `id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `first_name` varchar(200) NOT NULL,
  `last_name` varchar(200) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `login_terakhir` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `aktivasi` int(11) NOT NULL,
  `aktif` int(11) NOT NULL,
  `image` varchar(200) NOT NULL,
  `hp` varchar(50) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(50) NOT NULL,
  `province` varchar(50) NOT NULL,
  `postcode` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `me_member`
--

INSERT INTO `me_member` (`id`, `email`, `first_name`, `last_name`, `pass`, `login_terakhir`, `aktivasi`, `aktif`, `image`, `hp`, `address`, `city`, `province`, `postcode`) VALUES
(3, 'deo@markdesign.net', 'sales', 'dv', '7c4a8d09ca3762af61e59520943dc26494f8941b', '2015-07-10 04:44:34', 0, 1, '', '584651561', 'ajasdklaj', 'akjdsklja', 'kjsalkdjl', 'akjsd'),
(2, 'deoryzpandu@gmail.com', 'deory pandu putra', 'wahyu', '7c4a8d09ca3762af61e59520943dc26494f8941b', '2015-07-10 03:31:34', 0, 1, '', '0854646464', 'jl test test', 'batu', 'Australian Capital Territory', '65656');

-- --------------------------------------------------------

--
-- Table structure for table `or_order`
--

CREATE TABLE `or_order` (
  `id` int(11) NOT NULL,
  `invoice_no` int(11) NOT NULL,
  `invoice_prefix` varchar(20) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `first_name` varchar(128) NOT NULL,
  `last_name` varchar(128) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(128) NOT NULL,
  `payment_first_name` varchar(128) NOT NULL,
  `payment_last_name` varchar(128) NOT NULL,
  `payment_company` varchar(128) NOT NULL,
  `payment_address_1` varchar(128) NOT NULL,
  `payment_address_2` varchar(128) NOT NULL,
  `payment_city` varchar(128) NOT NULL,
  `payment_postcode` varchar(128) NOT NULL,
  `payment_zone` varchar(128) NOT NULL,
  `payment_country` varchar(128) NOT NULL,
  `shipping_first_name` varchar(128) NOT NULL,
  `shipping_last_name` varchar(128) NOT NULL,
  `shipping_company` varchar(128) NOT NULL,
  `shipping_address_1` varchar(128) NOT NULL,
  `shipping_address_2` varchar(128) NOT NULL,
  `shipping_city` varchar(128) NOT NULL,
  `shipping_postcode` varchar(128) NOT NULL,
  `shipping_zone` varchar(128) NOT NULL,
  `shipping_area` int(11) NOT NULL,
  `shipping_country` varchar(128) NOT NULL,
  `comment` text NOT NULL,
  `tax` decimal(15,4) NOT NULL,
  `total` decimal(15,4) NOT NULL,
  `order_status_id` int(11) NOT NULL,
  `affiliate_id` int(11) NOT NULL,
  `commission` decimal(15,4) NOT NULL,
  `language_id` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `currency_code` varchar(100) NOT NULL,
  `currency_value` decimal(15,4) NOT NULL,
  `ip` varchar(128) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_modif` datetime NOT NULL,
  `delivery_from` varchar(100) NOT NULL,
  `delivery_to` varchar(100) NOT NULL,
  `delivery_package` varchar(100) NOT NULL,
  `delivery_price` int(11) NOT NULL,
  `payment_method_id` int(11) NOT NULL,
  `delivery_weight` int(11) NOT NULL,
  `token` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `or_order`
--

INSERT INTO `or_order` (`id`, `invoice_no`, `invoice_prefix`, `customer_id`, `customer_group_id`, `first_name`, `last_name`, `email`, `phone`, `payment_first_name`, `payment_last_name`, `payment_company`, `payment_address_1`, `payment_address_2`, `payment_city`, `payment_postcode`, `payment_zone`, `payment_country`, `shipping_first_name`, `shipping_last_name`, `shipping_company`, `shipping_address_1`, `shipping_address_2`, `shipping_city`, `shipping_postcode`, `shipping_zone`, `shipping_area`, `shipping_country`, `comment`, `tax`, `total`, `order_status_id`, `affiliate_id`, `commission`, `language_id`, `currency_id`, `currency_code`, `currency_value`, `ip`, `date_add`, `date_modif`, `delivery_from`, `delivery_to`, `delivery_package`, `delivery_price`, `payment_method_id`, `delivery_weight`, `token`) VALUES
(3, 5342, 'DV-20150618', 2, 0, 'deory pandu putra', 'wahyu', 'deoryzpandu@gmail.com', '0854646464', 'deory pandu putra', 'wahyu', '', 'jl test', '', 'batu', '65656', 'Australian Capital Territory', '', 'deory pandu putra', 'wahyu', '', 'jl test', '', 'batu', '65656', 'Australian Capital Territory', 3, '', '', 13.4545, 123.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', '', 25, 0, 10000, ''),
(4, 8189, 'DV-20150618', 2, 0, 'deory pandu putra', 'wahyu', 'deoryzpandu@gmail.com', '0854646464', 'deory pandu putra', 'wahyu', '', 'jl test', '', 'batu', '65656', 'Australian Capital Territory', '', 'deory pandu putra', 'wahyu', '', 'jl test', '', 'batu', '65656', 'Australian Capital Territory', 6, '', '', 14.2727, 123.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', '', 34, 0, 10000, '');

-- --------------------------------------------------------

--
-- Table structure for table `or_order_history`
--

CREATE TABLE `or_order_history` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_status_id` int(11) NOT NULL,
  `notify` tinyint(4) NOT NULL,
  `comment` text NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `or_order_history`
--

INSERT INTO `or_order_history` (`id`, `member_id`, `order_id`, `order_status_id`, `notify`, `comment`, `date_add`) VALUES
(3, 2, 3, 1, 0, 'Your order DV-20150618-5342 successfully placed with status \"Pending\"', '2015-06-18 09:01:43'),
(4, 2, 4, 1, 0, 'Your order DV-20150618-8189 successfully placed with status \"Pending\"', '2015-06-18 09:02:25');

-- --------------------------------------------------------

--
-- Table structure for table `or_order_product`
--

CREATE TABLE `or_order_product` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` varchar(200) NOT NULL,
  `name` varchar(256) NOT NULL,
  `kode` varchar(256) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` decimal(15,4) NOT NULL,
  `total` decimal(15,4) NOT NULL,
  `attributes_id` int(11) NOT NULL,
  `attributes_name` varchar(256) NOT NULL,
  `attributes_price` decimal(15,4) NOT NULL,
  `berat` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `or_order_product`
--

INSERT INTO `or_order_product` (`id`, `order_id`, `product_id`, `image`, `name`, `kode`, `qty`, `price`, `total`, `attributes_id`, `attributes_name`, `attributes_price`, `berat`) VALUES
(3, 3, 213, 'e6c97-11867.jpg', 'HP - 15 g040 au', 'HP-15g040au', 1, 123.0000, 123.0000, 1, 'test', 123.0000, 10000),
(4, 4, 213, 'e6c97-11867.jpg', 'HP - 15 g040 au', 'HP-15g040au', 1, 123.0000, 123.0000, 1, 'test', 123.0000, 10000);

-- --------------------------------------------------------

--
-- Table structure for table `or_order_status`
--

CREATE TABLE `or_order_status` (
  `order_status_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `or_order_status`
--

INSERT INTO `or_order_status` (`order_status_id`, `name`) VALUES
(2, 'Processing'),
(3, 'Shipped'),
(7, 'Canceled'),
(5, 'Complete'),
(8, 'Denied'),
(9, 'Canceled Reversal'),
(10, 'Failed'),
(11, 'Refunded'),
(12, 'Reversed'),
(13, 'Chargeback'),
(1, 'Pending'),
(16, 'Voided'),
(15, 'Processed'),
(14, 'Expired'),
(17, 'Paid');

-- --------------------------------------------------------

--
-- Table structure for table `pg_blog`
--

CREATE TABLE `pg_blog` (
  `id` int(11) NOT NULL,
  `topik_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `active` int(11) NOT NULL,
  `date_input` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `insert_by` varchar(255) NOT NULL,
  `last_update_by` varchar(255) NOT NULL,
  `writer` int(25) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pg_blog`
--

INSERT INTO `pg_blog` (`id`, `topik_id`, `image`, `active`, `date_input`, `date_update`, `insert_by`, `last_update_by`, `writer`) VALUES
(1, 16, '7e0ec-5.jpg', 1, '2014-10-27 15:12:19', '2014-11-06 18:50:37', 'ibnu@markdesign.net', 'deoryzpandu@gmail.com', 32),
(2, 16, 'ebc90-91.jpg', 1, '2014-10-27 15:28:51', '2014-11-06 18:50:51', 'ibnu@markdesign.net', 'deoryzpandu@gmail.com', 32),
(5, 3, '3c1cf-example-article.jpg', 1, '2013-10-30 10:48:42', '2014-10-28 10:48:42', 'ibnu@markdesign.net', 'ibnu@markdesign.net', 32),
(4, 16, '1d897-Demarco-Latest-Bridal-Jewelry-Rings-Design-2013-5.jpg', 1, '2014-08-30 10:45:52', '2014-11-06 18:50:18', 'ibnu@markdesign.net', 'deoryzpandu@gmail.com', 32),
(6, 16, 'beac3-Bridal-Necklace-jewelry-hd-wallpapers-top-desktop-jewelry-images-in-widescreen.jpg', 1, '2014-11-05 16:51:02', '2014-11-06 18:28:03', 'deoryzpandu@gmail.com', 'deoryzpandu@gmail.com', 1),
(7, 16, '45739-custom-jewelry-design-300x300.jpeg', 1, '2014-11-06 18:25:55', '2014-11-06 18:43:45', 'deoryzpandu@gmail.com', 'deoryzpandu@gmail.com', 1),
(8, 16, '6ac2a-Bridal-Necklace-jewelry-hd-wallpapers-top-desktop-jewelry-images-in-widescreen.jpg', 1, '2014-11-06 18:26:56', '2014-11-06 18:43:23', 'deoryzpandu@gmail.com', 'deoryzpandu@gmail.com', 1),
(9, 18, '9cfbc-Diamond_Jewelry_Gold_Jewellery_Diamond_Rings.jpg', 1, '2014-11-06 18:44:24', '2014-11-06 18:44:24', 'deoryzpandu@gmail.com', 'deoryzpandu@gmail.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pg_blog_description`
--

CREATE TABLE `pg_blog_description` (
  `id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pg_blog_description`
--

INSERT INTO `pg_blog_description` (`id`, `blog_id`, `language_id`, `title`, `content`) VALUES
(25, 2, 2, 'manajemen kualifikasi', '<p>\r\n	    test aja\r\n</p>'),
(23, 4, 2, 'test data on few month ago', '<p>\r\n	   asfasdfasdfasdfasdfasdfadfadf\r\n</p>'),
(7, 5, 2, 'test data on few year ago', '<p>\r\n	aklsdjflkajsdflkajsfkjskdjflkasjflkjsafkjlasdf\r\n</p>'),
(13, 6, 2, 'Model minute: Tips cantik ala Raisa untuk daily activity!', '<p>\r\n	   Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic, odio quis accusamus officiis ipsam libero quaerat distinctio delectus accusantium, ullam fuga consequuntur provident? Voluptate facere architecto aut deleniti a mollitia, eaque blanditiis recusandae tenetur libero minima quod beatae hic dolorem.\r\n</p>\r\n<p>\r\n	   Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic, odio quis accusamus officiis ipsam libero quaerat distinctio delectus accusantium, ullam fuga consequuntur provident? Voluptate facere architecto aut deleniti a mollitia, eaque blanditiis recusandae tenetur libero minima quod beatae hic dolorem.\r\n</p>'),
(16, 7, 2, 'Bagaimana tampil sederhana, namun tetap “Fabulous” seperti Chelsea Islan', '<p>\r\n	  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam  convallis mauris et arcu bibendum, in placerat nisi semper. Vivamus eu  risus aliquam, vehicula magna eu, sodales elit. Quisque pellentesque est  sed dolor fermentum, eget porta diam ultricies. Mauris id viverra diam.  Vestibulum pretium purus accumsan molestie bibendum. Nullam sed purus  vitae mi viverra volutpat. Vestibulum vulputate metus quis velit semper  venenatis. Curabitur sed sapien justo. Integer eu urna lorem. Phasellus  ac auctor quam. Vivamus gravida posuere tortor, eu placerat metus  tincidunt non. Nulla ultrices eu purus quis mattis. Praesent ut orci  molestie, molestie leo ac, ornare est. Maecenas ornare leo faucibus,  rutrum diam a, finibus risus. Nunc congue laoreet porttitor.\r\n</p>\r\n<p>\r\n	  Nulla ullamcorper ipsum vel dui egestas  mattis. Aenean ut convallis augue. Integer nec tempor nibh. Fusce quis  odio id est tempor mollis vitae at ex. In ut est pellentesque, porttitor  est eu, eleifend lectus. Sed sollicitudin leo non ante elementum  ultricies. Quisque vel varius orci, in elementum urna.\r\n</p>'),
(15, 8, 2, 'Model minute: Tips cantik ala Raisa untuk daily activity!', '<p>\r\n	   Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam  convallis mauris et arcu bibendum, in placerat nisi semper. Vivamus eu  risus aliquam, vehicula magna eu, sodales elit. Quisque pellentesque est  sed dolor fermentum, eget porta diam ultricies. Mauris id viverra diam.  Vestibulum pretium purus accumsan molestie bibendum. Nullam sed purus  vitae mi viverra volutpat. Vestibulum vulputate metus quis velit semper  venenatis. Curabitur sed sapien justo. Integer eu urna lorem. Phasellus  ac auctor quam. Vivamus gravida posuere tortor, eu placerat metus  tincidunt non. Nulla ultrices eu purus quis mattis. Praesent ut orci  molestie, molestie leo ac, ornare est. Maecenas ornare leo faucibus,  rutrum diam a, finibus risus. Nunc congue laoreet porttitor.\r\n</p>\r\n<p>\r\n	   Nulla ullamcorper ipsum vel dui egestas  mattis. Aenean ut convallis augue. Integer nec tempor nibh. Fusce quis  odio id est tempor mollis vitae at ex. In ut est pellentesque, porttitor  est eu, eleifend lectus. Sed sollicitudin leo non ante elementum  ultricies. Quisque vel varius orci, in elementum urna.\r\n</p>'),
(17, 9, 2, 'Masquerade party? How to “still” look fabulous with nude pallete.', '<p>\r\n	    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam   convallis mauris et arcu bibendum, in placerat nisi semper. Vivamus eu   risus aliquam, vehicula magna eu, sodales elit. Quisque pellentesque est   sed dolor fermentum, eget porta diam ultricies. Mauris id viverra  diam.  Vestibulum pretium purus accumsan molestie bibendum. Nullam sed  purus  vitae mi viverra volutpat. Vestibulum vulputate metus quis velit  semper  venenatis. Curabitur sed sapien justo. Integer eu urna lorem.  Phasellus  ac auctor quam. Vivamus gravida posuere tortor, eu placerat  metus  tincidunt non. Nulla ultrices eu purus quis mattis. Praesent ut  orci  molestie, molestie leo ac, ornare est. Maecenas ornare leo  faucibus,  rutrum diam a, finibus risus. Nunc congue laoreet porttitor.\r\n</p>\r\n<p>\r\n	    Nulla ullamcorper ipsum vel dui egestas  mattis. Aenean ut convallis  augue. Integer nec tempor nibh. Fusce quis  odio id est tempor mollis  vitae at ex. In ut est pellentesque, porttitor  est eu, eleifend lectus.  Sed sollicitudin leo non ante elementum  ultricies. Quisque vel varius  orci, in elementum urna.\r\n</p>'),
(24, 1, 2, 'coba lagi', '<p>\r\n	   anda belum beruntung\r\n</p>');

-- --------------------------------------------------------

--
-- Table structure for table `prd_brand`
--

CREATE TABLE `prd_brand` (
  `id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `active` int(11) NOT NULL,
  `date_input` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `insert_by` varchar(255) NOT NULL,
  `last_update_by` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `prd_brand_description`
--

CREATE TABLE `prd_brand_description` (
  `id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `prd_category`
--

CREATE TABLE `prd_category` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT 0,
  `sort` int(11) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `data` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prd_category`
--

INSERT INTO `prd_category` (`id`, `parent_id`, `sort`, `image`, `type`, `data`) VALUES
(1, 0, NULL, '6fbd1-49b7d78fbfhome2-s1-icon.png', 'category', NULL),
(2, 0, NULL, '2a78d-a5edfe7540home2-s2-icon.png', 'category', NULL),
(3, 0, NULL, 'bfe22-2de648206fhome2-s3-icon.png', 'category', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `prd_category_description`
--

CREATE TABLE `prd_category_description` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT 0,
  `language_id` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `data` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prd_category_description`
--

INSERT INTO `prd_category_description` (`id`, `category_id`, `language_id`, `name`, `data`) VALUES
(12, 1, 3, 'WOODWORKING', NULL),
(14, 2, 3, 'PLYWOOD', NULL),
(13, 2, 2, 'PLYWOOD', NULL),
(16, 3, 3, 'WOODEN DOORS', NULL),
(15, 3, 2, 'WOODEN DOORS', NULL),
(11, 1, 2, 'WOODWORKING', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `prd_product`
--

CREATE TABLE `prd_product` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT 0,
  `brand_id` int(11) NOT NULL DEFAULT 0,
  `image` varchar(200) DEFAULT NULL,
  `kode` varchar(50) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `harga_coret` int(11) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `berat` int(11) DEFAULT NULL,
  `terbaru` int(11) DEFAULT NULL,
  `terlaris` int(11) DEFAULT NULL,
  `out_stock` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `date_input` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `data` text DEFAULT NULL,
  `tag` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prd_product`
--

INSERT INTO `prd_product` (`id`, `category_id`, `brand_id`, `image`, `kode`, `harga`, `harga_coret`, `stock`, `berat`, `terbaru`, `terlaris`, `out_stock`, `status`, `date`, `date_input`, `date_update`, `data`, `tag`) VALUES
(1, 1, 0, '36461-products_n_ifura_20.jpg', '23423432', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 10:49:11', '2020-05-22 10:49:11', '2020-06-04 15:41:46', 's:21:\"s:13:\"s:6:\"a:0:{}\";\";\";', 'WOODWORKING'),
(2, 1, 0, '63ceb-products_n_ifura_19.jpg', '326541', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 10:51:43', '2020-05-22 10:51:43', '2020-06-04 15:41:51', 's:13:\"s:6:\"a:0:{}\";\";', 'WOODWORKING'),
(3, 1, 0, '37b4a-products_n_ifura_18.jpg', '121364', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 10:54:35', '2020-05-22 10:54:35', '2020-06-04 15:41:56', 's:13:\"s:6:\"a:0:{}\";\";', 'WOODWORKING'),
(4, 1, 0, 'e7f8a-products_n_ifura_17.jpg', '65846115', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 11:02:42', '2020-05-22 11:02:42', '2020-06-04 15:42:01', 's:13:\"s:6:\"a:0:{}\";\";', 'WOODWORKING'),
(5, 1, 0, '1c207-products_n_ifura_16.jpg', '15656', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 11:05:41', '2020-05-22 11:05:41', '2020-06-04 15:42:07', 's:13:\"s:6:\"a:0:{}\";\";', 'WOODWORKING'),
(6, 1, 0, '75c59-products_n_ifura_15.jpg', '85848', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 11:17:00', '2020-05-22 11:17:00', '2020-06-04 15:42:14', 's:13:\"s:6:\"a:0:{}\";\";', 'WOODWORKING'),
(7, 1, 0, '252ab-products_n_ifura_14.jpg', '8154645', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 11:18:24', '2020-05-22 11:18:24', '2020-06-04 15:42:20', 's:13:\"s:6:\"a:0:{}\";\";', 'WOODWORKING'),
(8, 1, 0, '57d07-products_n_ifura_13.jpg', '4656464', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 11:19:53', '2020-05-22 11:19:53', '2020-06-04 15:42:26', 's:13:\"s:6:\"a:0:{}\";\";', 'WOODWORKING'),
(9, 1, 0, '2bd6a-products_n_ifura_12.jpg', '5465486', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 13:10:57', '2020-05-22 13:10:57', '2020-06-04 15:42:36', 's:29:\"s:21:\"s:13:\"s:6:\"a:0:{}\";\";\";\";', 'WOODWORKING'),
(10, 1, 0, 'b7b1c-products_n_ifura_11.jpg', '1861661', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 13:26:26', '2020-05-22 13:26:26', '2020-06-04 15:42:53', 's:13:\"s:6:\"a:0:{}\";\";', 'WOODWORKING'),
(11, 1, 0, '6b5d5-products_n_ifura_10.jpg', '548456', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 13:26:32', '2020-05-22 13:29:05', '2020-05-22 13:29:05', 'a:0:{}', ''),
(12, 1, 0, 'f793e-products_n_ifura_9.jpg', '6456561', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 13:29:51', '2020-05-22 13:32:29', '2020-05-22 13:32:29', 'a:0:{}', ''),
(13, 1, 0, '017f7-products_n_ifura_8.jpg', '156165', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 13:34:03', '2020-05-22 13:36:00', '2020-05-22 13:36:00', 'a:0:{}', ''),
(14, 1, 0, '892f7-products_n_ifura_7.jpg', '5565556', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 13:37:53', '2020-05-22 13:38:05', '2020-05-22 13:38:05', 'a:0:{}', ''),
(15, 2, 0, 'b6e2a-products_n_ifura_6.jpg', '9949449', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 13:39:58', '2020-05-22 13:39:58', '2020-06-04 15:44:27', 's:6:\"a:0:{}\";', 'PLYWOOD'),
(16, 2, 0, '392b9-products_n_ifura_5.jpg', '14184988', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 13:42:29', '2020-05-22 13:42:29', '2020-06-04 15:44:36', 's:6:\"a:0:{}\";', 'PLYWOOD'),
(17, 2, 0, '43fcb-products_n_ifura_4.jpg', '48484', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 13:52:45', '2020-05-22 13:52:45', '2020-06-04 15:44:45', 's:6:\"a:0:{}\";', 'PLYWOOD'),
(18, 2, 0, 'ff250-products_n_ifura_3.jpg', '419846', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 13:54:41', '2020-05-22 13:54:41', '2020-06-04 15:44:54', 's:6:\"a:0:{}\";', 'PLYWOOD'),
(19, 2, 0, '0a82a-products_n_ifura_2.jpg', '165649', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 13:57:19', '2020-05-22 13:57:19', '2020-06-04 15:43:59', 's:13:\"s:6:\"a:0:{}\";\";', 'PLYWOOD'),
(20, 2, 0, 'd6b0b-products_n_ifura_1.jpg', '548564', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-05-22 13:58:52', '2020-05-22 13:58:52', '2020-06-04 15:45:12', 's:13:\"s:6:\"a:0:{}\";\";', 'PLYWOOD');

-- --------------------------------------------------------

--
-- Table structure for table `prd_product_attributes`
--

CREATE TABLE `prd_product_attributes` (
  `id` int(11) NOT NULL,
  `id_str` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `attribute` varchar(200) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `prd_product_color`
--

CREATE TABLE `prd_product_color` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `image_color` varchar(200) DEFAULT NULL,
  `label` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `prd_product_description`
--

CREATE TABLE `prd_product_description` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT 0,
  `language_id` int(11) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `desc` text DEFAULT NULL,
  `varian` longtext DEFAULT NULL,
  `application` longtext DEFAULT NULL,
  `meta_title` varchar(200) DEFAULT NULL,
  `meta_desc` text DEFAULT NULL,
  `meta_key` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prd_product_description`
--

INSERT INTO `prd_product_description` (`id`, `product_id`, `language_id`, `name`, `desc`, `varian`, `application`, `meta_title`, `meta_desc`, `meta_key`) VALUES
(71, 7, 3, 'Merbau FJL Board Stair Thread', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm</p><p>18 x 1220 x 2440 mm</p>', '<p>15 x 1220 x 2440 mm</p><p>18 x 1220 x 2440 mm</p>', NULL, NULL, NULL),
(74, 9, 2, 'Merbau E4E Decking', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(75, 9, 3, 'Merbau E4E Decking', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.</p>', NULL, NULL, NULL),
(76, 10, 2, 'Merbau FJL Board', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(77, 10, 3, 'Merbau FJL Board', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm</p><p>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.</p>', NULL, NULL, NULL),
(20, 11, 2, 'Merbau Reeded Decking', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(21, 11, 3, 'Merbau Reeded Decking', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm</p><p>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.</p>', NULL, NULL, NULL),
(22, 12, 2, 'Merbau Clip Decking', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm</p><p>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways</p>', NULL, NULL, NULL),
(23, 12, 3, 'Merbau Clip Decking', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.</p>', NULL, NULL, NULL),
(24, 13, 2, 'Merbau Clip Decking', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(25, 13, 3, 'Merbau Clip Decking', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm</p><p>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.</p>', NULL, NULL, NULL),
(26, 14, 2, 'Merbau Solid Flooring T&G', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(27, 14, 3, 'Merbau Solid Flooring T&G', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm</p><p>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.</p>', NULL, NULL, NULL),
(84, 15, 2, 'Costum Size Falcata LVL Stick', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(85, 15, 3, 'Costum Size Falcata LVL Stick', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm</p><p>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.</p>', NULL, NULL, NULL),
(86, 16, 2, 'Falcata FJL Board', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(87, 16, 3, 'Falcata FJL Board', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.<br></p>', '<p>15 x 1220 x 2440 mm</p><p>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.</p>', NULL, NULL, NULL),
(88, 17, 2, 'Plywood Falcata 7.5 - 11.5mm', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(89, 17, 3, 'Plywood Falcata 7.5 - 11.5mm', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm</p><p>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.</p>', NULL, NULL, NULL),
(90, 18, 2, 'Plywood Falcata 12-23mm', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(91, 18, 3, 'Plywood Falcata 12-23mm', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.</p>', NULL, NULL, NULL),
(83, 19, 3, 'BlockBoard Falcata', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.<br></p>', NULL, NULL, NULL),
(93, 20, 3, 'BlockBoard MLH', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm</p><p>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.</p>', NULL, NULL, NULL),
(58, 1, 2, 'Merbau Solid Flooring T&G', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(59, 1, 3, 'Merbau Solid Flooring T&G', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm</p><p>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.</p>', NULL, NULL, NULL),
(60, 2, 2, 'Merbau FJL Bread Loaf Handrail', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(61, 2, 3, 'Merbau FJL Bread Loaf Handrail', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm</p><p>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.</p>', NULL, NULL, NULL),
(62, 3, 2, 'Merbau FJL Ladies Waist Handrail', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(63, 3, 3, 'Merbau FJL Ladies Waist Handrail', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm</p><p>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.</p>', NULL, NULL, NULL),
(64, 4, 2, 'Merbau Solid Bottom Rail', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(65, 4, 3, 'Merbau Solid Bottom Rail', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm</p><p>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.</p>', NULL, NULL, NULL),
(66, 5, 2, 'Merbau FJL Post', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(67, 5, 3, 'Merbau FJL Post', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm</p><p>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.</p>', NULL, NULL, NULL),
(68, 6, 2, 'Merbau FJL Beams', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(69, 6, 3, 'Merbau FJL Beams', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm</p><p>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.</p>', NULL, NULL, NULL),
(70, 7, 2, 'Merbau FJL Board Stair Thread', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(72, 8, 2, 'Merbau AntiSlip Decking', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(73, 8, 3, 'Merbau AntiSlip Decking', '<p>IFURA Blockboard MLH menggunakan kayu legal terdaftar Indonesia yang berkelanjutan adalah salah satu jenis papan kayu lapis majemuk kami yang direkayasa dimana strip kayu lunak disatukan dari ujung ke ujung dan diikat dengan permesinan, struktur sandwich yang menempatkan kayu keras di antara mereka. Produk kayu Blockboard IFURA hadir dengan berbagai ukuran dan ketebalan.</p>', '<p>15 x 1220 x 2440 mm</p><p>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard digunakan untuk membuat pintu, meja, rak, panel dan dinding partisi. Ini biasanya digunakan untuk penggunaan interior, karena jenis lem yang digunakan. Untuk mencapai kekuatan maksimum, IFURA memastikan inti berjalan memanjang.</p>', NULL, NULL, NULL),
(82, 19, 2, 'BlockBoard Falcata', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL),
(92, 20, 2, 'BlockBoard MLH', '<p>IFURA Blockboard MLH using Indonesian sustainable registered legal wood is one type of our engineered compound plywood board where softwood strips are joined edge to edge and bonded together machining, a sandwich structure placing hardwood between them. IFURA Blockboard wood product came with different sizes and thickness.</p>', '<p>15 x 1220 x 2440 mm<br>18 x 1220 x 2440 mm</p>', '<p>IFURA Blockboard is used to make doors, tables, shelves, paneling and partition walls. It is normally used for interior usages, due to the type of glues used. To achieve maximum strength, IFURA ensures that the core runs lengthways.</p>', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `prd_product_image`
--

CREATE TABLE `prd_product_image` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `label` varchar(200) NOT NULL,
  `value` text NOT NULL,
  `type` varchar(100) NOT NULL,
  `hide` int(11) NOT NULL,
  `group` varchar(100) NOT NULL,
  `dual_language` enum('n','y') NOT NULL,
  `sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `name`, `label`, `value`, `type`, `hide`, `group`, `dual_language`, `sort`) VALUES
(1, 'default_meta_title', 'Title', '', 'text', 0, 'default_meta', 'y', 1),
(2, 'default_meta_keywords', 'Keywords', '', 'textarea', 0, 'default_meta', 'y', 2),
(3, 'default_meta_description', 'Description', '', 'textarea', 0, 'default_meta', 'y', 3),
(4, 'google_tools_webmaster', 'Google Webmaster Code', '', 'textarea', 0, 'google_tools', 'n', 4),
(5, 'google_tools_analytic', 'Google Analytic Code', '', 'textarea', 0, 'google_tools', 'n', 5),
(6, 'purechat_status', 'Show Hide Widget', '', 'select', 0, 'purechat', 'n', 1),
(7, 'purechat_code', 'PureChat Code', '', 'textarea', 0, 'purechat', 'n', 1),
(8, 'invoice_start_number', 'Invoice Start Number', '1000', 'text', 0, 'invoice', 'n', 0),
(9, 'invoice_increment', 'Invoice Increment', '5', 'text', 0, 'invoice', 'n', 0),
(10, 'invoice_auto_cancel_after', 'Invoice Auto Cancel After', '72', 'text', 0, 'invoice', 'n', 0),
(11, 'lang_deff', 'Language Default', 'en', 'text', 0, 'data', 'n', 0),
(12, 'email', 'email', 'info@halim.com', 'text', 0, 'data', 'n', 1);

-- --------------------------------------------------------

--
-- Table structure for table `setting_description`
--

CREATE TABLE `setting_description` (
  `id` int(11) NOT NULL,
  `setting_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sl_slide`
--

CREATE TABLE `sl_slide` (
  `id` int(11) NOT NULL,
  `topik_id` int(11) DEFAULT 0,
  `image` varchar(255) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `date_input` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `insert_by` varchar(255) DEFAULT NULL,
  `last_update_by` varchar(255) DEFAULT NULL,
  `writer` varchar(200) DEFAULT NULL,
  `urutan` int(4) DEFAULT NULL,
  `image2` varchar(225) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sl_slide`
--

INSERT INTO `sl_slide` (`id`, `topik_id`, `image`, `active`, `date_input`, `date_update`, `insert_by`, `last_update_by`, `writer`, `urutan`, `image2`) VALUES
(1, 0, 'b0fa3-slide-1.jpg', 1, '2020-06-16 19:17:22', '2020-06-16 19:17:22', 'info@markdesign.net', 'info@markdesign.net', NULL, NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `sl_slide_description`
--

CREATE TABLE `sl_slide_description` (
  `id` int(11) NOT NULL,
  `slide_id` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sl_slide_description`
--

INSERT INTO `sl_slide_description` (`id`, `slide_id`, `language_id`, `title`, `content`, `url`) VALUES
(1, 1, 2, 'fcs 1', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_group`
--

CREATE TABLE `tb_group` (
  `id` int(11) NOT NULL,
  `group` varchar(50) NOT NULL,
  `aktif` int(11) NOT NULL,
  `akses` blob NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_group`
--

INSERT INTO `tb_group` (`id`, `group`, `aktif`, `akses`) VALUES
(8, 'Administrator', 1, 0x613a33373a7b693a303b733a31363a2261646d696e2e757365722e696e646578223b693a313b733a31373a2261646d696e2e757365722e637265617465223b693a323b733a31373a2261646d696e2e757365722e757064617465223b693a333b733a31373a2261646d696e2e757365722e64656c657465223b693a343b733a31373a2261646d696e2e736c6964652e696e646578223b693a353b733a31383a2261646d696e2e736c6964652e637265617465223b693a363b733a31383a2261646d696e2e736c6964652e757064617465223b693a373b733a31383a2261646d696e2e736c6964652e64656c657465223b693a383b733a31363a2261646d696e2e62616e6b2e696e646578223b693a393b733a31373a2261646d696e2e62616e6b2e637265617465223b693a31303b733a31373a2261646d696e2e62616e6b2e757064617465223b693a31313b733a31373a2261646d696e2e62616e6b2e64656c657465223b693a31323b733a31393a2261646d696e2e73657474696e672e696e646578223b693a31333b733a31383a2261646d696e2e6d656d6265722e696e646578223b693a31343b733a31393a2261646d696e2e6d656d6265722e637265617465223b693a31353b733a31393a2261646d696e2e6d656d6265722e757064617465223b693a31363b733a31393a2261646d696e2e6d656d6265722e64656c657465223b693a31373b733a31373a2261646d696e2e6f726465722e696e646578223b693a31383b733a31383a2261646d696e2e6f726465722e637265617465223b693a31393b733a31383a2261646d696e2e6f726465722e757064617465223b693a32303b733a31383a2261646d696e2e6f726465722e64656c657465223b693a32313b733a31373a2261646d696e2e6f726465722e7072696e74223b693a32323b733a32313a2261646d696e2e73657474696e672e636f6e74616374223b693a32333b733a31393a2261646d696e2e73657474696e672e61626f7574223b693a32343b733a32303a2261646d696e2e63617465676f72792e696e646578223b693a32353b733a32313a2261646d696e2e63617465676f72792e637265617465223b693a32363b733a32313a2261646d696e2e63617465676f72792e757064617465223b693a32373b733a32313a2261646d696e2e63617465676f72792e64656c657465223b693a32383b733a31393a2261646d696e2e73657474696e672e686f77746f223b693a32393b733a31393a2261646d696e2e70726f647563742e696e646578223b693a33303b733a32303a2261646d696e2e70726f647563742e637265617465223b693a33313b733a32303a2261646d696e2e70726f647563742e757064617465223b693a33323b733a32303a2261646d696e2e70726f647563742e64656c657465223b693a33333b733a32303a2261646d696e2e64656c69766572792e696e646578223b693a33343b733a32313a2261646d696e2e64656c69766572792e637265617465223b693a33353b733a32313a2261646d696e2e64656c69766572792e757064617465223b693a33363b733a32313a2261646d696e2e64656c69766572792e64656c657465223b7d);

-- --------------------------------------------------------

--
-- Table structure for table `tb_menu_akses`
--

CREATE TABLE `tb_menu_akses` (
  `id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `controller` varchar(50) NOT NULL,
  `action` blob NOT NULL,
  `sub_action` blob NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_menu_akses`
--

INSERT INTO `tb_menu_akses` (`id`, `type`, `name`, `controller`, `action`, `sub_action`) VALUES
(22, 'admin', 'User', 'user', 0x613a343a7b733a353a22696e646578223b733a393a224c6973742044617461223b733a363a22637265617465223b733a31313a224372656174652044617461223b733a363a22757064617465223b733a31313a225570646174652044617461223b733a363a2264656c657465223b733a31313a2244656c6574652044617461223b7d, 0x613a303a7b7d),
(21, 'admin', 'Slide', 'slide', 0x613a343a7b733a353a22696e646578223b733a393a224c6973742044617461223b733a363a22637265617465223b733a31313a224372656174652044617461223b733a363a22757064617465223b733a31313a225570646174652044617461223b733a363a2264656c657465223b733a31313a2244656c6574652044617461223b7d, 0x613a303a7b7d),
(40, 'admin', 'Bank', 'bank', 0x613a343a7b733a353a22696e646578223b733a393a224c6973742044617461223b733a363a22637265617465223b733a31313a224372656174652044617461223b733a363a22757064617465223b733a31313a225570646174652044617461223b733a363a2264656c657465223b733a31313a2244656c6574652044617461223b7d, 0x613a303a7b7d),
(18, 'admin', 'Setting', 'setting', 0x613a313a7b733a353a22696e646578223b733a31373a22456469742053657474696e6720556d756d223b7d, 0x613a303a7b7d),
(39, 'admin', 'Member', 'member', 0x613a343a7b733a353a22696e646578223b733a393a224c6973742044617461223b733a363a22637265617465223b733a31313a224372656174652044617461223b733a363a22757064617465223b733a31313a225570646174652044617461223b733a363a2264656c657465223b733a31313a2244656c6574652044617461223b7d, 0x613a303a7b7d),
(38, 'admin', 'Order', 'order', 0x613a353a7b733a353a22696e646578223b733a393a224c6973742044617461223b733a363a22637265617465223b733a31313a224372656174652044617461223b733a363a22757064617465223b733a31313a225570646174652044617461223b733a363a2264656c657465223b733a31313a2244656c6574652044617461223b733a353a227072696e74223b733a353a225072696e74223b7d, 0x613a303a7b7d),
(32, 'admin', 'Contact Us', 'setting', 0x613a313a7b733a373a22636f6e74616374223b733a32323a2245646974205061676520487562756e6769204b616d69223b7d, 0x613a303a7b7d),
(13, 'admin', 'About Us', 'setting', 0x613a313a7b733a353a2261626f7574223b733a31303a22456469742041626f7574223b7d, 0x613a303a7b7d),
(37, 'admin', 'Category', 'category', 0x613a343a7b733a353a22696e646578223b733a393a224c6973742044617461223b733a363a22637265617465223b733a31313a224372656174652044617461223b733a363a22757064617465223b733a31313a225570646174652044617461223b733a363a2264656c657465223b733a31313a2244656c6574652044617461223b7d, 0x613a303a7b7d),
(36, 'admin', 'How To Order', 'setting', 0x613a313a7b733a353a22686f77746f223b733a31323a22486f7720546f204f72646572223b7d, 0x613a303a7b7d),
(30, 'admin', 'Products', 'product', 0x613a343a7b733a353a22696e646578223b733a393a224c6973742044617461223b733a363a22637265617465223b733a31313a224372656174652044617461223b733a363a22757064617465223b733a31313a225570646174652044617461223b733a363a2264656c657465223b733a31313a2244656c6574652044617461223b7d, 0x613a303a7b7d),
(41, 'admin', 'Delivery Price', 'delivery', 0x613a343a7b733a353a22696e646578223b733a393a224c6973742044617461223b733a363a22637265617465223b733a31313a224372656174652044617461223b733a363a22757064617465223b733a31313a225570646174652044617461223b733a363a2264656c657465223b733a31313a2244656c6574652044617461223b7d, 0x613a303a7b7d);

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `type` varchar(50) NOT NULL,
  `group_id` int(11) NOT NULL,
  `login_terakhir` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `aktivasi` int(11) NOT NULL,
  `aktif` int(11) NOT NULL,
  `user_input` varchar(200) NOT NULL,
  `tanggal_input` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `initial` varchar(255) NOT NULL,
  `image` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `email`, `nama`, `pass`, `type`, `group_id`, `login_terakhir`, `aktivasi`, `aktif`, `user_input`, `tanggal_input`, `initial`, `image`) VALUES
(30, 'info@markdesign.net', 'admin', '564fda17f517ae04a86734c2b2341327ed4fd565', 'root', 8, '2020-06-16 12:10:11', 0, 1, '', '0000-00-00 00:00:00', 'Admin', ''),
(35, 'ibnudrift@gmail.com', 'ibnu fajar', '564fda17f517ae04a86734c2b2341327ed4fd565', 'root', 8, '2019-08-15 07:30:36', 0, 1, '', '0000-00-00 00:00:00', 'ibnu', '');

-- --------------------------------------------------------

--
-- Table structure for table `tt_text`
--

CREATE TABLE `tt_text` (
  `id` int(11) NOT NULL,
  `category` varchar(100) NOT NULL,
  `message` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tt_text`
--

INSERT INTO `tt_text` (`id`, `category`, `message`) VALUES
(1, 'admin', 'Produk'),
(2, 'admin', 'Pages'),
(3, 'admin', 'Orders'),
(4, 'admin', 'Customers'),
(5, 'admin', 'Promotions'),
(6, 'admin', 'Reports'),
(7, 'admin', 'General Setting'),
(8, 'admin', 'Data Edited'),
(9, 'admin', 'New Orders'),
(10, 'admin', 'New Customers'),
(11, 'admin', 'Payment Confirmation'),
(12, 'admin', 'Edit Profile'),
(13, 'admin', 'Change Password'),
(14, 'admin', 'Sign Out'),
(15, 'admin', 'Gallery'),
(16, 'admin', 'Slide Home'),
(17, 'admin', 'Toko'),
(18, 'admin', 'Slides'),
(19, 'admin', 'Product'),
(20, 'admin', 'Products'),
(21, 'admin', 'About Us'),
(22, 'admin', 'Contact Us'),
(23, 'admin', 'Trip'),
(24, 'admin', 'Trips'),
(25, 'admin', 'Slide'),
(26, 'admin', 'Healty'),
(27, 'admin', 'ge-ma'),
(28, 'admin', 'Our Services'),
(29, 'admin', 'Services Content'),
(30, 'admin', 'Projects'),
(31, 'admin', 'Peojects'),
(32, 'admin', 'Gallerys'),
(33, 'admin', 'Our Quality'),
(34, 'admin', 'Home'),
(35, 'admin', 'Career');

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_category`
-- (See below for the actual view)
--
CREATE TABLE `view_category` (
`id` int(11)
,`parent_id` int(11)
,`sort` int(11)
,`image` varchar(200)
,`type` varchar(100)
,`data` text
,`id2` int(11)
,`category_id` int(11)
,`language_id` int(11)
,`name` varchar(100)
,`data2` text
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_gallery`
-- (See below for the actual view)
--
CREATE TABLE `view_gallery` (
`id` int(11)
,`topik_id` int(11)
,`image` varchar(255)
,`image2` varchar(225)
,`active` int(11)
,`date_input` datetime
,`date_update` datetime
,`insert_by` varchar(255)
,`last_update_by` varchar(255)
,`writer` varchar(200)
,`city` varchar(100)
,`id2` int(11)
,`gallery_id` int(11)
,`language_id` int(11)
,`title` varchar(255)
,`sub_title` text
,`content` text
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_slide`
-- (See below for the actual view)
--
CREATE TABLE `view_slide` (
`id` int(11)
,`topik_id` int(11)
,`image` varchar(255)
,`active` int(11)
,`date_input` datetime
,`date_update` datetime
,`insert_by` varchar(255)
,`last_update_by` varchar(255)
,`writer` varchar(200)
,`id2` int(11)
,`slide_id` int(11)
,`language_id` int(11)
,`title` varchar(255)
,`content` text
,`url` varchar(200)
);

-- --------------------------------------------------------

--
-- Structure for view `view_category`
--
DROP TABLE IF EXISTS `view_category`;

CREATE ALGORITHM=UNDEFINED DEFINER=`richmore`@`localhost` SQL SECURITY DEFINER VIEW `view_category`  AS  select `prd_category`.`id` AS `id`,`prd_category`.`parent_id` AS `parent_id`,`prd_category`.`sort` AS `sort`,`prd_category`.`image` AS `image`,`prd_category`.`type` AS `type`,`prd_category`.`data` AS `data`,`prd_category_description`.`id` AS `id2`,`prd_category_description`.`category_id` AS `category_id`,`prd_category_description`.`language_id` AS `language_id`,`prd_category_description`.`name` AS `name`,`prd_category_description`.`data` AS `data2` from (`prd_category` join `prd_category_description` on(`prd_category`.`id` = `prd_category_description`.`category_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `view_gallery`
--
DROP TABLE IF EXISTS `view_gallery`;

CREATE ALGORITHM=UNDEFINED DEFINER=`richmore`@`localhost` SQL SECURITY DEFINER VIEW `view_gallery`  AS  select `gal_gallery`.`id` AS `id`,`gal_gallery`.`topik_id` AS `topik_id`,`gal_gallery`.`image` AS `image`,`gal_gallery`.`image2` AS `image2`,`gal_gallery`.`active` AS `active`,`gal_gallery`.`date_input` AS `date_input`,`gal_gallery`.`date_update` AS `date_update`,`gal_gallery`.`insert_by` AS `insert_by`,`gal_gallery`.`last_update_by` AS `last_update_by`,`gal_gallery`.`writer` AS `writer`,`gal_gallery`.`city` AS `city`,`gal_gallery_description`.`id` AS `id2`,`gal_gallery_description`.`gallery_id` AS `gallery_id`,`gal_gallery_description`.`language_id` AS `language_id`,`gal_gallery_description`.`title` AS `title`,`gal_gallery_description`.`sub_title` AS `sub_title`,`gal_gallery_description`.`content` AS `content` from (`gal_gallery` join `gal_gallery_description` on(`gal_gallery`.`id` = `gal_gallery_description`.`gallery_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `view_slide`
--
DROP TABLE IF EXISTS `view_slide`;

CREATE ALGORITHM=UNDEFINED DEFINER=`richmore`@`localhost` SQL SECURITY DEFINER VIEW `view_slide`  AS  select `sl_slide`.`id` AS `id`,`sl_slide`.`topik_id` AS `topik_id`,`sl_slide`.`image` AS `image`,`sl_slide`.`active` AS `active`,`sl_slide`.`date_input` AS `date_input`,`sl_slide`.`date_update` AS `date_update`,`sl_slide`.`insert_by` AS `insert_by`,`sl_slide`.`last_update_by` AS `last_update_by`,`sl_slide`.`writer` AS `writer`,`sl_slide_description`.`id` AS `id2`,`sl_slide_description`.`slide_id` AS `slide_id`,`sl_slide_description`.`language_id` AS `language_id`,`sl_slide_description`.`title` AS `title`,`sl_slide_description`.`content` AS `content`,`sl_slide_description`.`url` AS `url` from (`sl_slide` join `sl_slide_description` on(`sl_slide_description`.`slide_id` = `sl_slide`.`id`)) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gal_gallery`
--
ALTER TABLE `gal_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gal_gallery_description`
--
ALTER TABLE `gal_gallery_description`
  ADD PRIMARY KEY (`id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `gal_gallery_image`
--
ALTER TABLE `gal_gallery_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`gallery_id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `me_member`
--
ALTER TABLE `me_member`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `or_order`
--
ALTER TABLE `or_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `or_order_history`
--
ALTER TABLE `or_order_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `or_order_product`
--
ALTER TABLE `or_order_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `or_order_status`
--
ALTER TABLE `or_order_status`
  ADD PRIMARY KEY (`order_status_id`);

--
-- Indexes for table `pg_blog`
--
ALTER TABLE `pg_blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pg_blog_description`
--
ALTER TABLE `pg_blog_description`
  ADD PRIMARY KEY (`id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `prd_brand`
--
ALTER TABLE `prd_brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prd_brand_description`
--
ALTER TABLE `prd_brand_description`
  ADD PRIMARY KEY (`id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `prd_category`
--
ALTER TABLE `prd_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `type` (`type`);

--
-- Indexes for table `prd_category_description`
--
ALTER TABLE `prd_category_description`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`,`language_id`);

--
-- Indexes for table `prd_product`
--
ALTER TABLE `prd_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `prd_product_attributes`
--
ALTER TABLE `prd_product_attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `prd_product_color`
--
ALTER TABLE `prd_product_color`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `prd_product_description`
--
ALTER TABLE `prd_product_description`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`,`language_id`);

--
-- Indexes for table `prd_product_image`
--
ALTER TABLE `prd_product_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting_description`
--
ALTER TABLE `setting_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sl_slide`
--
ALTER TABLE `sl_slide`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sl_slide_description`
--
ALTER TABLE `sl_slide_description`
  ADD PRIMARY KEY (`id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `tb_group`
--
ALTER TABLE `tb_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_menu_akses`
--
ALTER TABLE `tb_menu_akses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `tt_text`
--
ALTER TABLE `tt_text`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gal_gallery`
--
ALTER TABLE `gal_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `gal_gallery_description`
--
ALTER TABLE `gal_gallery_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `gal_gallery_image`
--
ALTER TABLE `gal_gallery_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `me_member`
--
ALTER TABLE `me_member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `or_order`
--
ALTER TABLE `or_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `or_order_history`
--
ALTER TABLE `or_order_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `or_order_product`
--
ALTER TABLE `or_order_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `or_order_status`
--
ALTER TABLE `or_order_status`
  MODIFY `order_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `pg_blog`
--
ALTER TABLE `pg_blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `pg_blog_description`
--
ALTER TABLE `pg_blog_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `prd_brand`
--
ALTER TABLE `prd_brand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `prd_brand_description`
--
ALTER TABLE `prd_brand_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `prd_category`
--
ALTER TABLE `prd_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `prd_category_description`
--
ALTER TABLE `prd_category_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `prd_product`
--
ALTER TABLE `prd_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `prd_product_attributes`
--
ALTER TABLE `prd_product_attributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `prd_product_color`
--
ALTER TABLE `prd_product_color`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `prd_product_description`
--
ALTER TABLE `prd_product_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `prd_product_image`
--
ALTER TABLE `prd_product_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `setting_description`
--
ALTER TABLE `setting_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sl_slide`
--
ALTER TABLE `sl_slide`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sl_slide_description`
--
ALTER TABLE `sl_slide_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_group`
--
ALTER TABLE `tb_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tb_menu_akses`
--
ALTER TABLE `tb_menu_akses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `tt_text`
--
ALTER TABLE `tt_text`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
