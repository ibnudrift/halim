<?php

class HomeController extends Controller
{

	public function actions()
	{
		return array(
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
		);
	}

	public function actionIndex()
	{
		$this->layout='//layouts/column1';

		$model = new ContactForm;
		$model->scenario = 'insert';

		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			$status = true;
	        $secret_key = "6Ld4Pt8ZAAAAAG4voPZiXRFvrmM8fLv4uJImSCNG";
	        $url_set = "https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR'];
	        $cURLConnection = curl_init();
			curl_setopt($cURLConnection, CURLOPT_URL, $url_set);
			curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
			$pn_result = curl_exec($cURLConnection);
			curl_close($cURLConnection);
			$response = json_decode($pn_result);

	        if($response->success==false)
	        {
	          $status = false;
	          $model->addError('verifyCode', 'Verify you are not robbot');
	        }

			if($status AND $model->validate())
			{
				// config email
				$messaged = $this->renderPartial('//mail/contact',array(
					'model'=>$model,
				),TRUE);
				$config = array(
					'to'=>array($model->email, $this->setting['email'], $this->setting['contact_email']),
					'subject'=>'['.Yii::app()->name.'] Contact from '.$model->email,
					'message'=>$messaged,
				);
				if ($this->setting['contact_cc']) {
					$config['cc'] = array($this->setting['contact_cc']);
				}
				if ($this->setting['contact_bcc']) {
					$config['bcc'] = array($this->setting['contact_bcc']);
				}

				Common::mail($config);

				Yii::app()->user->setFlash('success','Thank you for contact us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		
		$this->render('index', array(
			'model' => $model,
		));
	}	

	public function actionAbout()
	{
		$this->layout='//layouts/column2';
		$this->pageTitle = 'About Us - '. $this->pageTitle;

		$this->render('about', array(
		));
	}	

	public function actionAgent()
	{
		$this->layout='//layouts/column2';
		$this->pageTitle = 'Become Our Partner - '. $this->pageTitle;

		$this->render('agent', array(
		));
	}	

	public function actionQuality()
	{
		$this->layout='//layouts/column2';
		$this->pageTitle = 'Quality Statement - '. $this->pageTitle;

		$this->render('quality', array(
		));
	}	

	public function actionContact()
	{
		$this->layout='//layouts/column2';
		$this->pageTitle = 'Contact - '. $this->pageTitle;

		$this->render('contact', array(
		));
	}	

	public function actionProduct()
	{
		$this->layout='//layouts/column2';
		$this->pageTitle = 'Products - '. $this->pageTitle;

		$this->render('product', array(
		));
	}

	public function actionProduct_detail()
	{
		$this->layout='//layouts/column2';
		$this->pageTitle = 'Products - '. $this->pageTitle;

		$this->render('product_detail', array(
		));
	}
	

}


