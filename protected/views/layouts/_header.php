<?php 
    $e_activemenu = $this->action->id;
    $controllers_ac = $this->id;
    $session=new CHttpSession;
    $session->open();
    $login_member = $session['login_member'];

    $active_menu_pg = $controllers_ac.'/'.$e_activemenu;
?>

<header class="head <?php if ($active_menu_pg != 'home/index'): ?>insides-head<?php endif ?>">
  <div class="prelative container cont-header mx-auto d-none d-sm-block">
    <div class="row">
      <div class="col-md-20 wow fadeInUp">
        <div class="image"><a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>"><img src="<?php echo $this->assetBaseurl; ?>logo-head.png" alt=""></a></div>
      </div>
      <div class="col-md-36 garis-header">
        <div class="menu-block-top wow fadeInUp">
          <div class="click change_lang">
            <p>
              <?php
              $get['lang'] = 'en';
              ?>
              <a class="active" href="<?php echo $this->createUrl($this->route, $get); ?>">EN</a>
              &nbsp;&nbsp;|&nbsp;&nbsp;
              <?php
              $get['lang'] = 'id';
              ?>
              <a href="<?php echo $this->createUrl($this->route, $get); ?>">IN</a>
            </p>
          </div>
        </div>
        <div class="menu-block-bottom wow fadeInUp">
          <ul class="list-inline">
            <li class="list-inline-item menu">
              <a href="<?php echo CHtml::normalizeUrl(array('/home/about', 'lang'=>Yii::app()->language)); ?>">About Us</a>
            </li>
            <li class="list-inline-item menu">
              <a href="<?php echo CHtml::normalizeUrl(array('/home/product', 'lang'=>Yii::app()->language)); ?>">Products</a>
            </li>
            <li class="list-inline-item menu">
              <a href="<?php echo CHtml::normalizeUrl(array('/home/quality', 'lang'=>Yii::app()->language)); ?>">Quality Statement</a>
            </li>
            <li class="list-inline-item menu">
              <a href="<?php echo CHtml::normalizeUrl(array('/home/agent', 'lang'=>Yii::app()->language)); ?>">become our partner</a>
            </li>
            <li class="list-inline-item menu">
              <a href="<?php echo CHtml::normalizeUrl(array('/home/contact', 'lang'=>Yii::app()->language)); ?>">Contact</a>
            </li>
          </ul>
        </div>
      </div>
      <div class="col-md-4 mx-auto">
        <div class="image-kanan-head icn_wa_head wow fadeInUp">
          <a href="https://wa.me/6281330666636"><img src="<?php echo $this->assetBaseurl; ?>lgo-wan-halim.png" alt=""></a>
        </div>
      </div>
    </div>
  </div>
  <div class="d-block d-sm-none">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="#"><img src="<?php echo $this->assetBaseurl; ?>logo-fx-res.png" class="img img-fluid" alt=""></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto m-0">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/about', 'lang'=>Yii::app()->language)); ?>">About Us</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/product', 'lang'=>Yii::app()->language)); ?>">Products</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/quality', 'lang'=>Yii::app()->language)); ?>">Quality Statement</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/agent', 'lang'=>Yii::app()->language)); ?>">become our partner</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/contact', 'lang'=>Yii::app()->language)); ?>">Contact</a>
          </li>
        </ul>
      </div>
    </nav>
  </div>
</header>

<section id="myAffix" class="header-affixs affix-top">
  <!-- <div class="clear height-15"></div> -->
  <div class="prelative container cont-header mx-auto">
    <div class="row">
      <div class="col-md-15 col-sm-15">
        <div class="lgo_web_headrs_wb">
          <a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>">
            <img src="<?php echo $this->assetBaseurl; ?>logo-head.png" alt="" class="img img-fluid">
          </a>
        </div>
      </div>
      <div class="col-md-45 col-sm-45">

        <div class="text-right"> 
          <div class="menu-taffix">
            <ul class="list-inline d-inline-block align-middle">
              <li class="list-inline-item menu">
                <a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>">Home</a>
              </li>
              <li class="list-inline-item menu">
                <a href="<?php echo CHtml::normalizeUrl(array('/home/about', 'lang'=>Yii::app()->language)); ?>">About Us</a>
              </li>
              <li class="list-inline-item menu">
                <a href="<?php echo CHtml::normalizeUrl(array('/home/product', 'lang'=>Yii::app()->language)); ?>">Products</a>
              </li>
              <li class="list-inline-item menu">
                <a href="<?php echo CHtml::normalizeUrl(array('/home/quality', 'lang'=>Yii::app()->language)); ?>">Quality Statement</a>
              </li>
              <li class="list-inline-item menu">
                <a href="<?php echo CHtml::normalizeUrl(array('/home/agent', 'lang'=>Yii::app()->language)); ?>">become our partner</a>
              </li>
              <li class="list-inline-item menu">
                <a href="<?php echo CHtml::normalizeUrl(array('/home/contact', 'lang'=>Yii::app()->language)); ?>">Contact</a>
              </li>
            </ul>

            <div class="image-kanan-head icn_wa_head d-inline-block align-middle ml-2 pl-3">
              <a href="https://wa.me/6281330666636"><img src="<?php echo $this->assetBaseurl; ?>lgo-wan-halim.png" style="max-width: 25px;" alt=""></a>
            </div>
          </div>
        </div>

      </div>
    </div>
    <div class="clear"></div>
  </div>
</section>

<script type="text/javascript">
  $(function(){

  var sn_width = $(window).width();
  if (sn_width > 1150) {

      $(window).scroll(function(){
        var sntop1 = $(window).scrollTop();

        if(sntop1 > 115){
          $('.header-affixs').removeClass('affix-top').addClass('affix');
        }else{
          $('.header-affixs.affix').removeClass('affix').addClass('affix-top');
        }
      });
    }

  });
</script>



<script>
  $(document).ready(function(){
    $("#headerproduct").css("display","none");
  });
</script>

<style type="text/css">
  header.head .image-kanan-head img{
    padding-top: 0;
  }
</style>