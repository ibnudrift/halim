<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<?php echo $this->renderPartial('//layouts/_header', array()); ?>


<!-- Start fcs -->
<?php
// $criteria = new CDbCriteria;
// $criteria->with = array('description');
// $criteria->addCondition('description.language_id = :language_id');
// $criteria->addCondition('active = 1');
// $criteria->params[':language_id'] = $this->languageID;
// $criteria->group = 't.id';
// $criteria->order = 't.id ASC';
// $slide = Slide::model()->with(array('description'))->findAll($criteria);
?>
<div class="fcs-wrapper outers_fcs_wrapper prelatife wrapper-slide">
    <div id="myCarousel_home" class="carousel slide" data-ride="carousel" data-interval="4500">
        <div class="carousel-inner">
            <div class="carousel-item active home-slider-new">
                <img class="img_fcs d-none d-sm-block" src="<?php echo $this->assetBaseurl; ?>slide/slide-1.jpg" alt="">
                <img class="img_fcs d-block d-sm-none" src="<?php echo $this->assetBaseurl; ?>slide/slide-1_res.jpg" alt="">
            </div>
            <div class="carousel-item home-slider-new">
                <img class="img_fcs d-none d-sm-block" src="<?php echo $this->assetBaseurl; ?>slide/slide-2.jpg" alt="">
                <img class="img_fcs d-block d-sm-none" src="<?php echo $this->assetBaseurl; ?>slide/slide-2_res.jpg" alt="">
            </div>
            <div class="carousel-item home-slider-new">
                <img class="img_fcs d-none d-sm-block" src="<?php echo $this->assetBaseurl; ?>slide/slide-3.jpg" alt="">
                <img class="img_fcs d-block d-sm-none" src="<?php echo $this->assetBaseurl; ?>slide/slide-3_res.jpg" alt="">
            </div>
            <div class="carousel-item home-slider-new">
                <img class="img_fcs d-none d-sm-block" src="<?php echo $this->assetBaseurl; ?>slide/slide-4.jpg" alt="">
                <img class="img_fcs d-block d-sm-none" src="<?php echo $this->assetBaseurl; ?>slide/slide-4_res.jpg" alt="">
            </div>
            <div class="carousel-item home-slider-new">
                <img class="img_fcs d-none d-sm-block" src="<?php echo $this->assetBaseurl; ?>slide/slide-5.jpg" alt="">
                <img class="img_fcs d-block d-sm-none" src="<?php echo $this->assetBaseurl; ?>slide/slide-5_res.jpg" alt="">
            </div>
            
            
        </div>

        <ol class="carousel-indicators">
            <li data-target="#myCarousel_home" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel_home" data-slide-to="1"></li>
            <li data-target="#myCarousel_home" data-slide-to="2"></li>
            <li data-target="#myCarousel_home" data-slide-to="3"></li>
            <li data-target="#myCarousel_home" data-slide-to="4"></li>
        </ol>

        <div class="carousel-caption caption-slider-home mx-auto">
            <div class="prelatife container mx-auto">
                <div class="bxsl_tx_fcs">
                    <div class="row no-gutters">
                        <div class="col-md-60 mx-auto text-center">
                            <div class="content wow fadeInUp">
                                <span style="text-transform:uppercase">
                                    <p>SOLE Distributor of Iberchem Fragrances & Scentium Flavours IN INDONESIA</p>
                                </span>
                                <h3>We bring flavors and fragrances that arouse your taste and make your life even more beautiful</h3>
                            </div>
                            
                            <div class="scroll">
                                <div class="row no-gutters">
                                    <div class="col-md-10">
                                        <a href="#">
                                        <img src="<?php echo $this->assetBaseurl; ?>Group 1.png">
                                        </a>
                                    </div>
                                    <div class="col-md-50">
                                        <a href="#"><p>Scroll Down</p></a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end caption -->

    </div>
</div>
</div>

<!-- End fcs -->

<?php echo $content ?>

<?php echo $this->renderPartial('//layouts/_footer', array()); ?>

<script type="text/javascript">
    $(document).ready(function() {

        if ($(window).width() >= 768) {
            var $item = $('#myCarousel_home.carousel .carousel-item'); 
            var $wHeight = $(window).height();
            $item.eq(0).addClass('active');
            $item.height($wHeight); 
            $item.addClass('full-screen');

            $('#myCarousel_home.carousel img.d-block d-sm-none').each(function() {
              var $src = $(this).attr('src');
              var $color = $(this).attr('data-color');
              $(this).parent().css({
                'background-image' : 'url(' + $src + ')',
                'background-color' : $color,
                'background-repeat' : 'no-repeat'
              });
              $(this).remove();
            });

            $(window).on('resize', function (){
              $wHeight = $(window).height();
              $item.height($wHeight);
            });

            $('#myCarousel_home.carousel').carousel({
              interval: 4000,
              pause: "false"
            });
        }else{
            var $item = $('#myCarousel_home.carousel .carousel-item'); 
            var $wHeight = $(window).height();
            $item.eq(0).addClass('active');
            $item.height($wHeight); 
            $item.addClass('full-screen');

            $('#myCarousel_home.carousel img.d-block.d-sm-none').each(function() {
              var $src = $(this).attr('src');
              var $color = $(this).attr('data-color');
              $(this).parent().css({
                'background-image' : 'url(' + $src + ')',
                'background-color' : $color,
                'background-repeat' : 'no-repeat'
              });
              $(this).remove();
            });

            $(window).on('resize', function (){
              $wHeight = $(window).height();
              $item.height($wHeight);
            });

            $('#myCarousel_home.carousel').carousel({
              interval: 4000,
              pause: "false"
            });
        }

    });
</script>

<script>
    $(document).ready(function() {
        $("#click").click(function() {
            $('html, body').animate({
                scrollTop: $("#div1").offset().top
            }, 2000);
        });
    });
</script>

<?php $this->endContent(); ?>