<?php 
$active_menu = $this->id.'/'.$this->action->id;
?>
<?php if ($active_menu != 'home/index'): ?>
<section class="footers_top">
	<div class="prelative container wow fadeInDown">
		<div class="py-4"></div>
		<div class="inner-footer py-5 text-center">
			<div class="lgo_footers text-center">
				<a href="#">
					<img src="<?php echo $this->assetBaseurl ?>lgo-footers_nx.png" alt="" class="img img-fluid d-block mx-auto text-center">
					<div class="d-block py-1 my-1"></div>
					<h4>CV. HALIM JAYA PUTRA</h4>
					<div class="d-block py-1"></div>
					<p>Distributor of Iberchem Fragrances & Scentium Flavours</p>
				</a>
			</div>
			<div class="py-4 my-2"></div>
			<div class="lists_socmed">
				<ul class="list-inline m-0">
					<li class="list-inline-item">
						<a href="https://wa.me/628971788888"><i class="fa fa-whatsapp"></i>
						<br>
						<span>Whatsapp</span></a>
					</li>
					<li class="list-inline-item">
						<a href="#"><i class="fa fa-map-marker"></i>
						<br>
						<span>Google Map</span></a>
					</li>
					<li class="list-inline-item">
						<a href="#"><i class="fa fa-instagram"></i>
						<br>
						<span>Instagram</span></a>
					</li>
				</ul>
			</div>
			<div class="py-3"></div>
			<div class="menus_footer">
				<ul class="list-inline">
					<li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/about', 'lang'=>Yii::app()->language)); ?>">ABOUT US</a></li>
					<li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/product', 'lang'=>Yii::app()->language)); ?>">PRODUCTS</a></li>
					<li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/quality', 'lang'=>Yii::app()->language)); ?>">QUALITY</a></li>
					<li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/agent', 'lang'=>Yii::app()->language)); ?>">BECOME AN AGENT</a></li>
					<li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/contact', 'lang'=>Yii::app()->language)); ?>">CONTACT</a></li>
				</ul>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</section>
<?php endif ?>

<section class="footer">
    <div class="prelative container-footer">
        <div class="row">
            <div class="col-md-60 wow fadeInDown">
                <p>Copyright &copy; 2020 CV. Halim Jaya Putra. All rights reserved. Website design by Mark Design Indonesia.</p>
            </div>
        </div>
    </div>
</section>