<section class="hero-inside-pages prelatife">
	<div class="picture_big"><img src="<?php echo $this->assetBaseurl ?>hero-contact.jpg" alt="" class="img img-fluid w-100"></div>
	<div class="caption-insides-top wow fadeInUp">
		<div class="inners">
			<h1>Contact - CV. Halim Jaya Putra</h1>
			<div class="py-2"></div>
			<p>SOLE Distributor of Iberchem Fragrances & Scentium Flavours IN INDONESIA</p>
			<div class="clear"></div>
		</div>
	</div>
</section>

<section class="middle_inside_wrap">

	<div class="block_outer_breadcrumbs back-white py-2">
		<div class="prelatife container wow fadeInDown">
			<div class="row py-1">
				<div class="col-md-40 col-40">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb m-0">
					    <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
					    <li class="breadcrumb-item active" aria-current="page">Contact</li>
					  </ol>
					</nav>
				</div>
				<div class="col-md-20 col-20">
					<div class="text-right backs-page">
						<a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>" class="btn btn-link p-0">BACK</a>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<!-- end breadcrumb -->

	<section class="backs_grey insides_topback1_qualitys py-5">
		<div class="prelatife container">
			<div class="inners py-5">
				<div class="content-text text-center tops_content_quality d-blcok mx-auto wow fadeInDown">
					<h2>
						Let’s Get In Touch
					</h2>
					<p>At Halim Jaya Putra, we believe in communication transparency.  So to be better providing any information regarding Iberchem and Scentium or even other information, pleae feel free to contact us through your desired methods of contact at this page. We will be ready to respond.</p>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</section>

	<section class="back-white backs_quality_sec3 py-5">
		<div class="prelatife container py-5">
			<div class="inners content-text text-center">

				<div class="row wow fadeInDown">
					<div class="col-md-7"></div>
					<div class="col-md-46">
						<div class="row address_text">
							<div class="col borders-r">
								<div class="texts">
									<h5>OFFICE HEADQUARTER</h5>
									<p>Ruko Sentra Taman Puspa Raya C2<br>Citraraya, Surabaya, Indonesia<br><a href="https://www.google.com/search?q=cv%20halim%20jaya%20putra%20surabaya&rlz=1C1CHBF_enID816ID816&oq=halim+jaya+putra+&aqs=chrome.2.69i57j0i22i30i457j0i22i30j69i60l3.4351j0j7&sourceid=chrome&ie=UTF-8&safe=strict&sxsrf=ALeKk01jAH6YYx9fTjQfTceKRG4-SQ7n_A:1604550729219&npsic=0&rflfq=1&rlha=0&rllag=-7297323,112676894,4299&tbm=lcl&rldimm=9544358671055355656&lqi=ChxjdiBoYWxpbSBqYXlhIHB1dHJhIHN1cmFiYXlhWjMKE2N2IGhhbGltIGpheWEgcHV0cmEiHGN2IGhhbGltIGpheWEgcHV0cmEgc3VyYWJheWE&ved=2ahUKEwjGv_ShyersAhVGWysKHbMIDFwQvS4wAXoECAEQLA&rldoc=1&tbs=lrf:!1m4!1u2!2m2!2m1!1e1!1m4!1u16!2m2!16m1!1e1!1m4!1u16!2m2!16m1!1e2!2m1!1e2!2m1!1e16!3sIAE,lf:1,lf_ui:2&rlst=f#rlfi=hd:;si:9544358671055355656,l,ChxjdiBoYWxpbSBqYXlhIHB1dHJhIHN1cmFiYXlhWjMKE2N2IGhhbGltIGpheWEgcHV0cmEiHGN2IGhhbGltIGpheWEgcHV0cmEgc3VyYWJheWE;mv:[[-7.272834199999999,112.7129363],[-7.321811899999999,112.64085259999999]];tbs:lrf:!1m4!1u2!2m2!2m1!1e1!1m4!1u16!2m2!16m1!1e1!1m4!1u16!2m2!16m1!1e2!2m1!1e2!2m1!1e16!3sIAE,lf:1,lf_ui:2" target="_blank">View On Google Map</a></p>
								</div>
							</div>
							<div class="col">
								<div class="texts">
									<h5>WAREHOUSE</h5>
									<p>Raya Menganti 252<br>Surabaya, 60828, Indonesia<br><a href="#" target="_blank">View On Google Map</a></p>
								</div>
							</div>
						</div>

						<div class="py-3"></div>
						<div class="lines-grey"></div>
						<div class="py-3 my-2"></div>

						<div class="address_text">
							<h5>CUSTOMER SERVICE HOTLINE</h5>
							<div class="py-2"></div>
							<ul class="list-inline">
								<li class="list-inline-item">
									<i class="fa fa-whatsapp"></i>
									<div class="py-1"></div>
									<p>CLICK TO CHAT<br>
									<a href="https://wa.me/6281330666636">081 330 6666 36</a>
								</li>
								<li class="list-inline-item">
									<i class="fa fa-envelope"></i>
									<div class="py-1"></div>
									<p>EMAIL <br>
									<a href="mailto:info@halimjayaputra.com"></a>info@halimjayaputra.com</p>
								</li>
								<li class="list-inline-item">
									<i class="fa fa-phone"></i>
									<div class="py-1"></div>
									<p>TELEPHONE<br>
									031 7420077</p>
								</li>
							</ul>
						</div>
						<div class="py-3"></div>

					</div>
					<div class="col-md-7"></div>
				</div>
				
				<div class="clear"></div>
			</div>
		</div>
	</section>

	<div class="clear"></div>
</section>