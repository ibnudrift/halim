<section class="home-sec-1">
	<div class="prelative container">
		<div class="title text-center wow fadeInDown">
			<p>Let’s Connect, Talk To Us</p>
		</div>
		<div class="connect wow fadeInDown">
			<div class="row">
				<div class="col-md-28">
					<div class="connect-logo">
						<a href="https://wa.me/628971788888">
							<img src="<?php echo $this->assetBaseurl; ?>WA.png">
							<p>Whatsapp Chat</p>
						</a>
					</div>
				</div>
				<div class="col-md-2"></div>
				<div class="col-md-28">
					<div class="connect-logo">
						<a href="">
							<img src="<?php echo $this->assetBaseurl; ?>pencil.png">
							<p>Online Inquiry</p>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="home-sec-2">
	<div class="prelative container2">
		<div class="row no-gutters">
			<div class="col-md-30">
				<div class="kiri wow fadeInLeft">
					<div class="title">
						<p>Iberchem & Scentium Distributor</p>
					</div>
					<div class="py-3">
						<img class="img-fluid d-block mx-auto" src="<?php echo $this->assetBaseurl; ?>lgn-banner-left-home1.png">
					</div>
					<p>Established since 2004 in Surabaya, Indonesia. CV. Halim Jaya Putra offers comprehensive range of exclusive fragrances and flavours to industries in Indonesia who are looking for the right and the best taste or smell for their products. CV. Halim Jaya Putra’s reputation is proven and we are proud to provide the most safe, innovative and quality tested products by Iberchem & Scentium (Murcia, Spain).<br><br>Feel free to browse our product variants or you can directly contact our hotline to get faster information.</p>

					<a href="<?php echo CHtml::normalizeUrl(array('/home/product', 'lang'=>Yii::app()->language)); ?>">
						<div class="button-product">
							<p>PRODUCTS</p>
						</div>
					</a>

				</div>
			</div>
			<div class="col-md-30">
				<div class="kanan wow fadeInRight">
					<img class="img-fluid" src="<?php echo $this->assetBaseurl; ?>banner-s1.jpg">
				</div>
			</div>
		</div>
	</div>
</section>

<section class="home-sec-3">
	<div class="prelative container2">
		<div class="row no-gutters">
			<div class="col-md-30 prelative">
				<div class="kiri wow fadeInLeft">
					<img class="img-fluid" src="<?php echo $this->assetBaseurl; ?>banner-s2.jpg">
				</div>
				<div class="tengah wow fadeInDown">
					<img class="img-fluid" src="<?php echo $this->assetBaseurl; ?>banner-s2_n_small.jpg">
				</div>
			</div>
			<div class="col-md-30">
				<div class="kanan wow fadeInRight">
					<div class="title text-left">
						<p>About Halim Jaya Putra</p>
					</div>
					<div class="py-3 d-none d-sm-block"></div>
					<div class="py-2 d-block d-sm-none"></div>
					<p>Born from the passion of presenting the best fragrance and flavour, CV. Halim Jaya Putra started to operate from Surabaya, the company quickly gained trusts from customers around the country. Since day one, our compassionate and comitted founders have advanced our culture: to walk together as dependable partner, become modern fragrance & flavour suppliers that understands the market and responding with smart products and promote the highest quality products to ensure successful end products on our client’s end. <br><br>Expect to be mezmerised with thousands of options and variations of fragrance by Iberchem and flavours by Scentium when you work with us, but no worries cause our specifier will help you all the way, <b><u>we are just one click away to chat.</b></u></p>
					<a href="<?php echo CHtml::normalizeUrl(array('/home/about', 'lang'=>Yii::app()->language)); ?>">
						<div class="button-about">
							<p>ABOUT US</p>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="home-sec-4">
	<div class="prelative container px-0">
		<div class="row">
			<div class="col-md-60">
				<div class="subtitle wow fadeInDown">
					<p>Distributor of Iberchem Fragrances & Scentium Flavours</p>
					<div class="py-1"></div>
					<h3>Halim Jaya Putra Quick Facts</h3>
				</div>
			</div>
			<div class="col-md-15">
				<div class="boxed">
					<div class="inner wow fadeInDown">
						<h4 class="counter" data-count="1750">1750+</h4>
						<p>Fragrance Variants</p>
					</div>
				</div>
			</div>
			<div class="col-md-15">
				<div class="boxed">
					<div class="inner wow fadeInDown">
						<h4 class="counter" data-count="1000">1000+</h4>
						<p>Flavour Variants</p>
					</div>
				</div>
			</div>
			<div class="col-md-15">
				<div class="boxed">
					<div class="inner wow fadeInDown">
						<h4 class="counter" data-count="250">250+</h4>
						<p>Industrial Consumers</p>
					</div>
				</div>
			</div>
			<div class="col-md-15">
				<div class="boxed">
					<div class="inner wow fadeInDown">
						<h4 class="counter" data-count="800">800+</h4>
						<p>Agents & Resellers</p>
					</div>
				</div>
			</div>
			<div class="formm">
				<div class="col-md-60">
					<div class="title wow fadeInDown">
						<p>IBERCHEM & SCENTIUM INQUIRY FORM</p>
					</div>
					<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
					'type'=>'',
					'enableAjaxValidation'=>false,
					'clientOptions'=>array(
					'validateOnSubmit'=>false,
					),
					'htmlOptions' => array(
						'enctype' => 'multipart/form-data',
						'class' => 'fielddd wow fadeInUp',
					),
					)); ?>
					<?php echo $form->errorSummary($model, '', '', array('class'=>'alert alert-danger')); ?>
					<?php if(Yii::app()->user->hasFlash('success')): ?>
					<?php $this->widget('bootstrap.widgets.TbAlert', array(
					'alerts'=>array('success'),
					)); ?>
					<?php endif; ?>
						<div class="form-row">
							<div class="form-group col-md-30">
								<label for="inputEmail4">YOUR NAME</label>
								<?php echo $form->textField($model, 'name', array('class'=>'form-control')); ?>
							</div>
							<div class="form-group col-md-30">
								<label for="inputEmail4">EMAIL</label>
								<?php echo $form->textField($model, 'email', array('class'=>'form-control')); ?>
							</div>
							<div class="form-group col-md-30">
								<label for="inputEmail4">MOBILE PHONE</label>
								<?php echo $form->textField($model, 'phone', array('class'=>'form-control')); ?>
							</div>
							<div class="form-group col-md-30">
								<label for="inputEmail4">ADDRESS</label>
								<?php echo $form->textField($model, 'address', array('class'=>'form-control')); ?>
							</div>
							<div class="form-group col-md-30">
								<label for="inp+utEmail4">COMPANY NAME</label>
								<?php echo $form->textField($model, 'company', array('class'=>'form-control')); ?>
							</div>
							<div class="form-group col-md-30">
								<label for="inputEmail4">CITY</label>
								<?php echo $form->textField($model, 'city', array('class'=>'form-control')); ?>
							</div>
							<div class="form-group col-md-60">
								<label for="inputEmail4">NOTES</label>
								<?php echo $form->textArea($model, 'body', array('class'=>'form-control', 'rows'=>3)); ?>
							</div>
							<div class="form-group col-md-30">
								<div class="g-recaptcha" data-sitekey="6Ld4Pt8ZAAAAALIUsZjVSPuxLOIBBKp0Qi-_X1xg"></div>
							</div>
							<div class="form-group col-md-30">
								<button type="submit" class="form-control">submit</button>
							</div>
						</div>
					<?php $this->endWidget(); ?>

				</div>
			</div>
		</div>

		<div class="footer">
			<div class="col-md-60">
				<div class="row">
					<div class="col-md-60 wow fadeInDown">
						<div class="title">
							<img class="img-fluid" src="<?php echo $this->assetBaseurl; ?>lgo-footers_nx.png">
							<h2>CV. HALIM JAYA PUTRA</h2>
							<p>Distributor of Iberchem Fragrances & Scentium Flavours</p>
						</div>
					</div>
					<div class="col-md-60 wow fadeInDown">
						<div class="lists_socmed text-center">
							<ul class="list-inline m-0">
								<li class="list-inline-item">
									<a href="https://wa.me/628971788888"><i class="fa fa-whatsapp"></i>
									<br>
									<span>Whatsapp</span></a>
								</li>
								<li class="list-inline-item">
									<a href="#"><i class="fa fa-map-marker"></i>
									<br>
									<span>Google Map</span></a>
								</li>
								<li class="list-inline-item">
									<a href="#"><i class="fa fa-instagram"></i>
									<br>
									<span>Instagram</span></a>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-md-60 wow fadeInDown">
						<div class="list">
							<ul>
								<li>
									<a href="<?php echo CHtml::normalizeUrl(array('/home/about', 'lang'=>Yii::app()->language)); ?>">ABOUT US</a>
									<a href="<?php echo CHtml::normalizeUrl(array('/home/product', 'lang'=>Yii::app()->language)); ?>">PRODUCTS</a>
									<a href="<?php echo CHtml::normalizeUrl(array('/home/quality', 'lang'=>Yii::app()->language)); ?>">QUALITY</a>
									<a href="<?php echo CHtml::normalizeUrl(array('/home/agent', 'lang'=>Yii::app()->language)); ?>">BECOME AN AGENT</a>
									<a href="<?php echo CHtml::normalizeUrl(array('/home/contact', 'lang'=>Yii::app()->language)); ?>">CONTACT</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</section>

<script src='https://www.google.com/recaptcha/api.js'></script>