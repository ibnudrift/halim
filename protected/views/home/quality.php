<section class="hero-inside-pages prelatife">
	<div class="picture_big"><img src="<?php echo $this->assetBaseurl ?>hero-quality.jpg" alt="" class="img img-fluid w-100"></div>
	<div class="caption-insides-top wow fadeInUp">
		<div class="inners">
			<h1>Quality Statement - CV. Halim Jaya Putra</h1>
			<div class="py-2"></div>
			<p>SOLE Distributor of Iberchem Fragrances & Scentium Flavours IN INDONESIA</p>
			<div class="clear"></div>
		</div>
	</div>
</section>

<section class="middle_inside_wrap">

	<div class="block_outer_breadcrumbs back-white py-2">
		<div class="prelatife container wow fadeInDown">
			<div class="row py-1">
				<div class="col-md-40 col-40">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb m-0">
					    <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>">Home</a></li>
					    <li class="breadcrumb-item active" aria-current="page">Quality</li>
					  </ol>
					</nav>
				</div>
				<div class="col-md-20 col-20">
					<div class="text-right backs-page">
						<a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>" class="btn btn-link p-0">BACK</a>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<!-- end breadcrumb -->

	<section class="backs_grey insides_topback1_qualitys py-5">
		<div class="prelatife container">
			<div class="inners pt-5">
				<div class="content-text text-center tops_content_quality d-blcok mx-auto wow fadeInDown">
					<h2>
						How We Manage & Perform <br>
						<small><b>Strict & Excellent Quality Control Process</b></small>
					</h2>
					<p>Born from the passion of presenting the best fragrance and flavour, CV. Halim Jaya Putra started to operate from Surabaya, the company quickly gained trusts from customers around the country. Since day one, our compassionate and comitted founders have advanced our culture: to walk together as dependable partner, become modern fragrance & flavour suppliers that understands the market and responding with smart products and promote the highest quality products to ensure successful end products on our client’s end. </p>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</section>

	<?php 
	$lists_pillar = [
						[
							'pict'=>'quality-banners_n1.png',	
							'title'=>'Ambition',	
							'desc'=>'Performing above expectations, taking initiative.',	
						],
						[
							'pict'=>'quality-banners_n2.png',
							'title'=>'Curiousity',	
							'desc'=>'Asking bold, brave questions, build knowledge. ',	
						],
						[
							'pict'=>'quality-banners_n3.png',
							'title'=>'Collaboration',	
							'desc'=>'Working together with mutual respect, partnership, and appreciation.',	
						],
						[
							'pict'=>'quality-banners_n4.png',
							'title'=>'Consciousnes',	
							'desc'=>'Being reliable and taking responsibility. ',	
						],
						
					];
	?>

	<section class="back-white backs_quality_sec2 py-5">
		<div class="prelatife container py-5">
			<div class="inners content-text text-center wow fadeInDown">

				<div class="lists_items_pil_quality">
					<div class="row">
						<?php foreach ($lists_pillar as $key => $value): ?>
						<div class="col-md-15 col-30">
							<div class="items">
								<div class="pict"><img src="<?php echo $this->assetBaseurl.$value['pict'] ?>" alt="" class="img img-fluid"></div>
								<div class="info">
									<h5><?php echo $value['title'] ?></h5>
									<p><?php echo $value['desc'] ?></p>
								</div>
							</div>
						</div>
						<?php endforeach ?>
					</div>
					<div class="clear"></div>	
				</div>

				<div class="clear"></div>
			</div>
		</div>
	</section>

	<section class="back-white backs_quality_sec3 py-5">
		<div class="prelatife container py-5">
			<div class="inners content-text text-center wow fadeInDown">
				<h3>Quality Is At Our Dna</h3>
				<div class="py-2"></div>
				<div class="row">
					<div class="col"><img src="<?php echo $this->assetBaseurl.'qualitys-banner-1.jpg' ?>" alt="" class="img img-fluid w-100"></div>
					<div class="col"><img src="<?php echo $this->assetBaseurl.'qualitys-banner-2.jpg' ?>" alt="" class="img img-fluid w-100"></div>
				</div>
				<div class="py-3"></div>
				<div class="row">
					<div class="col-md-5"></div>
					<div class="col-md-50">
						<p>At CV. Halim Jaya Putra we work not just merely as a sole agent and distributor of Iberchem and Scentium, instead we are working to build connections and solutions with our market consumers. The only thing that make all relationship last is trust. CV. Halim Jaya Putra build trusts through a consistent product and service quality. We apply our experience and knowledge to learn and improve quality for both service and product that we offer.</p>
						<p>CV. Halim Jaya Putra have continuously invested and developed the quality division to improve and increase national distribution demand in a sustainable way. Through this expertise, we have also managed to build - and - create additional market possibilities, by studying each local trends and preferences.</p>
						<p>National and International competition challenges are real and will not vanish on their own. The only thing that matter will be our commitment to not do any trade-off between building a profitable business and decreasing value. We are committed to make a difference, performing to proove our mission, vision and values embody CV Halim Jaya Putra’s spirit and DNA.</p>
					</div>
					<div class="col-md-5"></div>
				</div>
				
				<div class="clear"></div>
			</div>
		</div>
	</section>

	<div class="clear"></div>
</section>