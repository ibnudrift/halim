<section class="hero-inside-pages prelatife">
	<div class="picture_big"><img src="<?php echo $this->assetBaseurl ?>hero-partner.jpg" alt="" class="img img-fluid w-100"></div>
	<div class="caption-insides-top wow fadeInUp">
		<div class="inners">
			<h1>Become Our Partner - CV. Halim Jaya Putra</h1>
			<div class="py-2"></div>
			<p>SOLE Distributor of Iberchem Fragrances & Scentium Flavours IN INDONESIA</p>
			<div class="clear"></div>
		</div>
	</div>
</section>

<section class="middle_inside_wrap">

	<div class="block_outer_breadcrumbs back-white py-2">
		<div class="prelatife container wow fadeInDown">
			<div class="row py-1">
				<div class="col-md-40 col-40">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb m-0">
					    <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
					    <li class="breadcrumb-item active" aria-current="page">Become Our Partner</li>
					  </ol>
					</nav>
				</div>
				<div class="col-md-20 col-20">
					<div class="text-right backs-page">
						<a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>" class="btn btn-link p-0">BACK</a>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<!-- end breadcrumb -->

	<section class="backs_grey insides_topback1_qualitys py-5 backs_agent">
		<div class="prelatife container">
			<div class="inners pt-5">
				<div class="content-text text-center tops_content_quality d-blcok mx-auto wow fadeInDown">
					<h2>
						Let’s Grow Together<br>
						<small><b>Your Success Starts Here!</b></small>
					</h2>
					<p>CV. Halim Jaya Putra will help you grow to be a successful partner of distribution. If you are a sales person, a store owner, or a supply distributor agent? Let’s talk to consider becoming an Iberchem and Scentium agent through CV. Halim Jaya Putra. </p>
					<p>We will be helping independent sales person and business owners grow their business while saving time and money. We have the product that people wanted, we will share the expertly designed strategy and work plan to ensure a quick and easy sales start. Along the way, you will be supported by CV. Halim Jaya Putra agent support team which will be there to resolve any issues and difficulties.</p>
					<p><strong>How we grow together as an agent of Iberchem Fragrances & Scentium Flavours:</strong></p>

				</div>
				
				<div class="py-3"></div>
				<div class="row lists_ban_items_agent wow fadeInDown">
					<div class="col-md-20">
						<div class="items">
							<div class="pict"><img src="<?php echo $this->assetBaseurl.'ban-agents-1.png' ?>" alt="" class="img img-fluid"></div>
							<div class="infos">
								<h6>Selection</h6>
								<p>Your business strategy will always be the focus and fuel that drive us. Curated products are the enabler. We believe the right selection of products will be your smart move that create great customer experiences and – subsequently generate loyalty.</p>
							</div>
						</div>
					</div>
					<div class="col-md-20">
						<div class="items">
							<div class="pict"><img src="<?php echo $this->assetBaseurl.'ban-agents-2.png' ?>" alt="" class="img img-fluid"></div>
							<div class="infos">
								<h6>Scalation</h6>
								<p>We are ready for the long run by standing to our commitment of product sustainability, continuity and availability. We bring ourselves to be a dependable partner to consumers by excessive product scaling to fulfil national demands.</p>
							</div>
						</div>
					</div>
					<div class="col-md-20">
						<div class="items">
							<div class="pict"><img src="<?php echo $this->assetBaseurl.'ban-agents-3.png' ?>" alt="" class="img img-fluid"></div>
							<div class="infos">
								<h6>Ambition</h6>
								<p>To move our business and recognition forward, we analyse and optimise each process experience, uncovering and filling any gaps, perfecting what’s imperfect. We learn, study to get the answer and develop specific product variation as the tools to reach our goals together.</p>
							</div>
						</div>
					</div>
				</div>
				<!-- end list row -->
				<div class="py-4"></div>

				<div class="clear"></div>
			</div>
		</div>
	</section>
