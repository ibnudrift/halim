<section class="hero-inside-pages prelatife">
	<div class="picture_big"><img src="<?php echo $this->assetBaseurl ?>hero-products.jpg" alt="" class="img img-fluid w-100"></div>
	<div class="caption-insides-top wow fadeInUp">
		<div class="inners">
			<h1>Products - CV. Halim Jaya Putra</h1>
			<div class="py-2"></div>
			<p>SOLE Distributor of Iberchem Fragrances & Scentium Flavours IN INDONESIA</p>
			<div class="clear"></div>
		</div>
	</div>
</section>

<section class="middle_inside_wrap">

	<div class="block_outer_breadcrumbs back-white py-2">
		<div class="prelatife container wow fadeInDown">
			<div class="row py-1">
				<div class="col-md-40 col-40">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb m-0">
					    <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>">Home</a></li>
					    <li class="breadcrumb-item active" aria-current="page">Products</li>
					  </ol>
					</nav>
				</div>
				<div class="col-md-20 col-20">
					<div class="text-right backs-page">
						<a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>" class="btn btn-link p-0">BACK</a>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<!-- end breadcrumb -->

	<section class="backs_grey insides_topback1_qualitys py-5 backs_agent">
		<div class="prelatife container">
			<div class="inners pt-5">
				<div class="content-text text-center tops_content_quality d-blcok mx-auto wow fadeInDown">
					<h2>
						Iberchem & Scentium Distributor
					</h2>
					<div class="py-2"></div>
					<div class="banners_mid"><img src="<?php echo $this->assetBaseurl ?>banners_ib_prds.jpg" alt="" class="img img-fluid d-block mx-auto"></div>
					<div class="py-2 my-1"></div>
					<p>CV. Halim Jaya Putra is the oficial Indonesian sole distributor of Iberchem and Scentium, both of these brands was set up in 1985 and is now present in over 120 countries. Both Iberchem and Scentium continue to invest for the improvement and expansion of operations, services and products for the increasingly competitive market that is the Fine Fragramce, Personal and Home Care, Fabric Care, Air Care and also the Food & Beverage industry.</p>
					<p><strong>Browse our product category below:</strong></p>
				<div class="clear"></div>
			</div>
			<div class="py-2"></div>

			<?php 
			$list_prd = [
							[
								'pict'=>'fine-fragrance-halim-iberchem-scentium.jpg',
								'name'=>'fine fragrance',
							],
							[
								'pict'=>'personal-care-halim-iberchem-scentium.jpg',
								'name'=>'personal care',
							],
							[
								'pict'=>'home-care-halim-iberchem-scentium.jpg',
								'name'=>'home care',
							],
							[
								'pict'=>'fabric-care-halim-iberchem-scentium.jpg',
								'name'=>'fabric care',
							],
							[
								'pict'=>'air-care-halim-iberchem-scentium.jpg',
								'name'=>'air care',
							],
							[
								'pict'=>'sweets-halim-iberchem-scentium.jpg',
								'name'=>'SWEETS',
							],
							[
								'pict'=>'beverage-halim-iberchem-scentium.jpg',
								'name'=>'beverages',
							],
							[
								'pict'=>'savoury-halim-iberchem-scentium.jpg',
								'name'=>'savoury',
							],
							
						];
			?>
			<div class="lists_prdcat_def text-center wow fadeInUp">
				<div class="row">
					<?php foreach ($list_prd as $key => $value): ?>
					<div class="col-md-15 col-30">
						<div class="items mb-3">
							<div class="pict">
								<a href="<?php echo CHtml::normalizeUrl(array('/home/product_detail', 'id'=> $key,  'name'=> strtolower($value['name']), 'lang'=>Yii::app()->language)); ?>"><img src="<?php echo $this->assetBaseurl ?>product/<?php echo $value['pict'] ?>" alt="" class="img img-fluid"></a>
							</div>
							<div class="info py-3">
								<a href="<?php echo CHtml::normalizeUrl(array('/home/product_detail', 'id'=> $key, 'name'=> strtolower($value['name']), 'lang'=>Yii::app()->language)); ?>"><h3><?php echo strtoupper($value['name']) ?></h3></a>
							</div>
						</div>
						<div class="py-1"></div>
					</div>
					<?php endforeach ?>
				</div>
			</div>

			<div class="clear"></div>
		</div>
		</div>
	</section>
