<section class="hero-inside-pages prelatife">
	<div class="picture_big"><img src="<?php echo $this->assetBaseurl ?>hero-about.jpg" alt="" class="img img-fluid w-100"></div>
	<div class="caption-insides-top wow fadeInUp">
		<div class="inners">
			<h1>Products - CV. Halim Jaya Putra</h1>
			<div class="py-2"></div>
			<p>SOLE Distributor of Iberchem Fragrances & Scentium Flavours IN INDONESIA</p>
			<div class="clear"></div>
		</div>
	</div>
</section>

<section class="middle_inside_wrap">

	<div class="block_outer_breadcrumbs back-white py-2">
		<div class="prelatife container wow fadeInDown">
			<div class="row py-1">
				<div class="col-md-40 col-40">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb m-0">
					    <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
					    <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/product')); ?>">Products</a></li>
					    <li class="breadcrumb-item active" aria-current="page"><?php echo ucwords($_GET['name']) ?></li>
					  </ol>
					</nav>
				</div>
				<div class="col-md-20 col-20">
					<div class="text-right backs-page">
						<a href="<?php echo CHtml::normalizeUrl(array('/home/product')); ?>" class="btn btn-link p-0">BACK</a>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<!-- end breadcrumb -->

	<?php 
			$list_prd = [
							[
								'pict'=>'fine-fragrance-halim-iberchem-scentium.jpg',
								'name'=>'fine fragrance',
								'desc'=>'<p>Please do consult to Halim Jaya Putra for further application for fine fragrance scent ingredients application such as EDT (Eau de Toilette), EDC (Eau de Cologne), EDP (Eau de Parfum), body mist, and so much more.</p><p>If you want to discover more extensive range or discuss about your exclusive custom / tailor-made product possibility, please contact us.</p>',
							],
							[
								'pict'=>'personal-care-halim-iberchem-scentium.jpg',
								'name'=>'personal care',
								'desc'=>'<p>Please do consult to Halim Jaya Putra for further application for Personal care scent ingredients application such as shampoo, face cream, body lotion, body shower, face toner and so much more.</p><p>If you want to discover more extensive range or discuss about your exclusive custom / tailor-made product possibility, please contact us.</p>',
							],
							[
								'pict'=>'home-care-halim-iberchem-scentium.jpg',
								'name'=>'home care',
								'desc'=>'<p>Please do consult to Halim Jaya Putra for further application for Home care ingredients application such as dish wash liquid, floor cleaner, glass cleaner and so much more.</p><p>If you want to discover more extensive range or discuss about your exclusive custom / tailor-made product possibility, please contact us.</p>',
							],
							[
								'pict'=>'fabric-care-halim-iberchem-scentium.jpg',
								'name'=>'fabric care',
								'desc'=>'<p>Please do consult to Halim Jaya Putra for further application for Fabric care ingredients application such as softener, liquid detergent, detergent and so much more.</p><p>If you want to discover more extensive range or discuss about your exclusive custom / tailor-made product possibility, please contact us.</p>',
							],
							[
								'pict'=>'air-care-halim-iberchem-scentium.jpg',
								'name'=>'air care',
								'desc'=>'<p>Please do consult to Halim Jaya Putra for further application for Air care ingredients application such as car freshener, air freshener, car perfume, aromatheraphy.</p><p>If you want to discover more extensive range or discuss about your exclusive custom / tailor-made product possibility, please contact us.</p>',
							],
							[
								'pict'=>'sweets-halim-iberchem-scentium.jpg',
								'name'=>'SWEETS',
								'desc'=>'<p>Please do consult to Halim Jaya Putra for further application for Sweets ingredients application such as candies, jellies, ice cream, gums and so much more.</p><p>If you want to discover more extensive range or discuss about your exclusive custom / tailor-made product possibility, please contact us.</p>',
							],
							[
								'pict'=>'beverage-halim-iberchem-scentium.jpg',
								'name'=>'beverages',
								'desc'=>'<p>Please do consult to Halim Jaya Putra for further application for Beverages ingredients application such as coffee, tea, milk and so much more.</p><p>If you want to discover more extensive range or discuss about your exclusive custom / tailor-made product possibility, please contact us.</p>',
							],
							[
								'pict'=>'savoury-halim-iberchem-scentium.jpg',
								'name'=>'savoury',
								'desc'=>'<p>Please do consult to Halim Jaya Putra for further application for Savoury ingredients application such as snacks and chips and so much more.</p><p>If you want to discover more extensive range or discuss about your exclusive custom / tailor-made product possibility, please contact us.</p>',
							],
							
						];

			$data_select = $list_prd[$_GET['id']];
			?>

	<section class="backs_grey insides_topback1_qualitys py-5 backs_agent">
		<div class="prelatife container">
			<div class="inners pt-5">
				<div class="content-text text-center tops_content_quality d-blcok mx-auto wow fadeInDown">
					<h2>
						<?php echo ucwords($_GET['name']) ?>
					</h2>
					<div class="py-2"></div>
					<div class="pictures">
						<img src="<?php echo $this->assetBaseurl .'product/'. $data_select['pict']; ?>" alt="<?php echo ucwords($_GET['name']) ?>" class="img img-fluid">
					</div>
					<div class="py-2 my-1"></div>
					<?php if ($_GET['id'] == 5 || $_GET['id'] == 6 || $_GET['id'] == 7): ?>
					<div class="banners_mid"><img src="<?php echo $this->assetBaseurl ?>scentium-logo-halim-jayaputra.jpg" alt="" class="img img-fluid d-block mx-auto"></div>
					<div class="py-2 my-1"></div>
					<p><strong>CV. Halim Jaya Putra presents Scentium flavor.</strong></p>
					<?php else: ?>
					<div class="banners_mid"><img src="<?php echo $this->assetBaseurl ?>bx_banners_lgos.jpg" alt="" class="img img-fluid d-block mx-auto"></div>
					<div class="py-2 my-1"></div>
					<p><strong>CV. Halim Jaya Putra presents Iberchem Fine Fragrance.</strong></p>
					<?php endif ?>

					<?php echo $data_select['desc'] ?>
						
					<div class="py-3"></div>
				<div class="clear"></div>
			</div>
			<div class="py-1"></div>

			<div class="mod_products table-responsive d-none hidden">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Product</th>
							<th>Category</th>
							<th>Application</th>
							<th>Similarity to</th>
						</tr>
					</thead>
					<tbody>
						<?php for ($i=1; $i < 11; $i++) { ?>
						<tr>
							<td>Product Name Here</td>
							<td>Fine Fragrance</td>
							<td>Perfume, Aromatherapy, Fabric</td>
							<td>Channel number 5</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>


			<div class="py-4 my-2"></div>
			<?php 
			$list_prd = [
							[
								'pict'=>'ban-itm-prd.jpg',
								'name'=>'fine fragrance',
							],
							[
								'pict'=>'ban-itm-prd.jpg',
								'name'=>'personal care',
							],
							[
								'pict'=>'ban-itm-prd.jpg',
								'name'=>'home care',
							],
							[
								'pict'=>'ban-itm-prd.jpg',
								'name'=>'fabric care',
							],
							[
								'pict'=>'ban-itm-prd.jpg',
								'name'=>'air care',
							],
							[
								'pict'=>'ban-itm-prd.jpg',
								'name'=>'SWEETS',
							],
							[
								'pict'=>'ban-itm-prd.jpg',
								'name'=>'beverages',
							],
							[
								'pict'=>'ban-itm-prd.jpg',
								'name'=>'savoury',
							],
							
						];
			?>
			<div class="text-center wow fadeInDown">
				<p><strong>Browse other our product category below:</strong></p>
				<div class="py-2"></div>
				<div class="lists_cat_prdDetails">
					<ul class="list-inline m-0">
						<?php foreach ($list_prd as $key => $value): ?>
						<li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/product_detail', 'id'=> $key,'name'=>strtolower($value['name']))); ?>"><?php echo strtoupper($value['name']) ?></a></li>
						<?php endforeach ?>
					</ul>
				</div>
			</div>
			<div class="py-1"></div>
			<div class="py-4"></div>
			<div class="clear"></div>
		</div>
		</div>
	</section>
