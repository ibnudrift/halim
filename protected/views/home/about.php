<section class="hero-inside-pages prelatife">
	<div class="picture_big"><img src="<?php echo $this->assetBaseurl ?>hero-about.jpg" alt="" class="img img-fluid w-100"></div>
	<div class="caption-insides-top">
		<div class="inners wow fadeInUp">
			<h1>About Us - CV. Halim Jaya Putra</h1>
			<div class="py-2"></div>
			<p>SOLE Distributor of Iberchem Fragrances & Scentium Flavours IN INDONESIA</p>
			<div class="clear"></div>
		</div>
	</div>
</section>

<section class="middle_inside_wrap">

	<div class="block_outer_breadcrumbs back-white py-2">
		<div class="prelatife container wow fadeInDown">
			<div class="row py-1">
				<div class="col-md-40 col-40">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb m-0">
					    <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>">Home</a></li>
					    <li class="breadcrumb-item active" aria-current="page">About Us</li>
					  </ol>
					</nav>
				</div>
				<div class="col-md-20 col-20">
					<div class="text-right backs-page">
						<a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>" class="btn btn-link p-0">BACK</a>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<!-- end breadcrumb -->

	<section class="backs_blue insides_topback1 py-5">
		<div class="prelatife container">
			<div class="inners py-4 wow fadeInDown">
				<div class="content-text text-center tops_content_about">
					<h2 class="small_title"><b>Scents & Fragrances...</b> <br>From the air that we breathe <br>to the food that we eat</h2>
					<p>These are our passion that drive our range of busines<br>and this is how what we do to complement them</p>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</section>

	<section class="back-white insides_topback2 py-5">
		<div class="prelatife container">
			<div class="inners py-5 wow fadeInDown">
				<div class="content-text text-left">
					<h3>About Halim Jaya Putra</h3>
					<div class="row">
						<div class="col-md-30">
							<p>Born from the passion of presenting the best fragrance and flavour, CV. Halim Jaya Putra started to operate from Surabaya, the company quickly gained trusts from customers around the country. Since day one, our compassionate and comitted founders have advanced our culture: to walk together as dependable partner, become modern fragrance & flavour suppliers that understands the market and responding with smart products and promote the highest quality products to ensure successful end products on our client’s end. </p>
							<p>CV. Halim Jaya Putra grows knowledge to responsibly feed the nation with better scent-taste and give the good breeze to the air that we breathe with better aroma. Supporting our commitment of bringing world with better taste, we pursue a strategy of bringin product with the highest value and consistency, therefore we gladly become Iberchem and Scentium sole distributor in Indonesia. CV. Halim Jaya Putra is promoting green, sustainable climate-friendly products for delivering our passion in the scent and fragrance industry.</p>
						</div>
						<div class="col-md-30">
							<div class="picture banners"><img src="<?php echo $this->assetBaseurl.'about-banner-1.jpg' ?>" alt="" class="img img-fluid"></div>
						</div>
					</div>

					<div class="py-4 my-3"></div>

					<div class="row">
						<div class="col-md-30">
							<div class="picture banners"><img src="<?php echo $this->assetBaseurl.'about-banner-2.jpg' ?>" alt="" class="img img-fluid"></div>
							<div class="d-block d-sm-none py-3"></div>
						</div>
						<div class="col-md-30">
							<h3>Our Goal</h3>
							<p>CV. Halim Jaya Putra’s ambition is to be the scent and fragrance company for the Future. We are committed to creating value for our customers, principals and society at large, as we work to develop quality product value and distributor agentship chain. To achieve our ambition, we have taken the lead in developing high end lab and tools for precision mixing, and work closely with partners throughout the agent-selling chain to improve and learn the new trends and sustainability of each scents and fragrances.</p>
							<p>Expect to be mezmerised with thousands of options and variations of fragrance by Iberchem and flavours by Scentium when you work with us, but no worries cause our specifier will help you all the way, <a href="#">we are just one click away to chat.</a></p>
						</div>
					</div>

				</div>
				<div class="py-3"></div>
				<div class="clear"></div>
			</div>
		</div>
	</section>
	
	<?php 
	$lists_pillar = [
						[
							'pict'=>'about-banner-pillar-1.png',	
							'title'=>'Quality',	
							'desc'=>'The highest investment ever to be in the company is the investment towards quality products and what we do to achieve exceptional product quality.',	
						],
						[
							'pict'=>'about-banner-pillar-2.png',
							'title'=>'Value',	
							'desc'=>'We bring value to every drop of our fragrances and scents products. We took the benefit of using the high quality materials, to bring product that imprints the taste preference of our customers.',	
						],
						[
							'pict'=>'about-banner-pillar-3.png',
							'title'=>'Diversity',	
							'desc'=>'We always aim to be different to be incomparable, through endless research and development to produce product diversification from time to time.',	
						],
						[
							'pict'=>'about-banner-pillar-4.png',
							'title'=>'People',	
							'desc'=>'Our people is the operator and one of the most important pillar. Dedication and consciousness are the definition suitable to address our people that operates this business.',	
						],
						
					];
	?>
	<section class="back-grey insides_topback3 py-5">
		<div class="prelatife container wow fadeInDown">
			<div class="inners py-5 content-text text-center">
				<h3>Halim Jaya Putra’s<br>
				Four Pillars of Success</h3>
				<div class="py-3"></div>
				<div class="lists_pillar_about">
					<div class="row">
						<?php foreach ($lists_pillar as $key => $value): ?>
						<div class="col-md-15 col-30">
							<div class="items">
								<div class="pict"><img src="<?php echo $this->assetBaseurl. $value['pict'] ?>" alt="" class="img img-fluid d-block mx-auto"></div>
								<div class="info pt-3">
									<h5><?php echo $value['title'] ?></h5>
									<div class="py-1"></div>
									<p><?php echo $value['desc'] ?></p>
								</div>
							</div>
						</div>
						<?php endforeach ?>
					</div>
				</div>
				<div class="clear clearfix"></div>
			</div>
		</div>
	</section>

	<section class="back-blues insides_topback4 py-5">
		<div class="prelatife container wow fadeInDown">
			<div class="inners py-5 content-text text-center">
				<h3>Vission & Mission</h3>
				<div class="py-3"></div>
				<div class="row">
					<div class="col-md-30 borders-r">
						<div class="texts">
						<h4 class="titles_visi">Vision</h4>
						<p>Our vision for the nation is based upon ensuring a better quality of life with better scents and fragrance. A collaborating corporate that answers the need of both culturals and trends, creating product solutions that will lead the market and become a respected corporate that sets the standards in the industry.</p>
						</div>
					</div>
					<div class="col-md-30">
						<div class="texts">
						<h4 class="titles_visi">Mission</h4>
						<p>Our mission is to define our company’s purpose and role in the widest range of scents and fragrances industry. We will aim to help feed the world with better taste, create fragrances that ignite positive senses, build economic empowerment and new innovative ideas to empower our chain of agentship and in the end enriching end product value to create profitable businesses.</p>
						</div>
					</div>
				</div>

				<div class="clear clearfix"></div>
			</div>
		</div>
	</section>

	<div class="clear"></div>
</section>